#!/bin/bash
set -e

sed -i "s|@DB_USER|$DB_USER|" ./src/main/resources/application-develop.yml
sed -i "s|@DB_PW|$DB_PW|" ./src/main/resources/application-develop.yml
sed -i "s|@DB_USER|$DB_USER|" ./src/main/resources/application-production.yml
sed -i "s|@DB_PW|$DB_PW|" ./src/main/resources/application-production.yml