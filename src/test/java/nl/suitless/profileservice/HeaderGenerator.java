package nl.suitless.profileservice;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HeaderGenerator {
    public static MultiValueMap<String, String> getDefaultHeaders() {
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + generateJwtToken("0353e6ae-a0da-4531-a0d9-1dbb1976641d"));

        return headers;
    }

    public static MultiValueMap<String, String> getHeaders(String userId) {
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + generateJwtToken(userId));

        return headers;
    }

    private static String generateJwtToken(String userId) {
        Date expirationDate = java.sql.Date.valueOf(LocalDate.now().plusDays(1));
        Date currentDate = Date.valueOf(LocalDate.now());
        List<String> scopes = new ArrayList<>();
        scopes.add("ADMIN");

        String subject = "integration@test.com";
        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("userID", userId);
        claims.put("scopes", scopes);

        return Jwts.builder()
                .setSubject(subject)
                .setClaims(claims)
                .setIssuedAt(currentDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, "Rt1WpRegdoIZl324WW9kn2aBZIskef7Z")
                .compact();
    }
}
