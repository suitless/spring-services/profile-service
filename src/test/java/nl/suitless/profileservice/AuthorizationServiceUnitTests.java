package nl.suitless.profileservice;

import nl.suitless.profileservice.Config.RoleManagement.RoleManagementConfig;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Exceptions.UnauthorizedAccount;
import nl.suitless.profileservice.Services.AuthorizationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationServiceUnitTests {

    private Collection<GrantedAuthority> authorities;

    @Mock
    private RoleManagementConfig roleManagementConfig;

    @InjectMocks
    private AuthorizationService authorizationService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        authorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("admin"));
    }

    @Test
    public void checkValid() {
        when(roleManagementConfig.getRoles()).thenReturn(new String[]{"admin", "editor"});

        authorizationService.check(authorities);

        verify(roleManagementConfig, times(1)).getRoles();
    }

    @Test(expected = UnauthorizedAccount.class)
    public void checkInvalid() {
        when(roleManagementConfig.getRoles()).thenReturn(new String[]{"user"});

        authorizationService.check(authorities);
    }

    @Test(expected = UnauthorizedAccount.class)
    public void checkIsOwnerInValid() {
        String accountId = "b1824801-003b-4298-8157-1d51f4e04c46";
        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("6253ac2b-55d0-4294-83dc-c0cfdcdb1222");
        BusinessProfile businessProfile = new BusinessProfile("TestBusiness",ownerIds);

        authorizationService.checkIsOwner(accountId, businessProfile);
    }
}
