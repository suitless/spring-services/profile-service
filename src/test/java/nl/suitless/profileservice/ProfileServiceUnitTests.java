package nl.suitless.profileservice;

import nl.suitless.profileservice.Data.IProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.ModuleReport;
import nl.suitless.profileservice.Domain.Entities.Profile.Todo;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.ProfileNotFoundException;
import nl.suitless.profileservice.Domain.Exceptions.TagAlreadyExistsException;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import nl.suitless.profileservice.Services.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceUnitTests {

    private Profile profile;
    private List<StringTag> stringTags;
    private List<DoubleTag> doubleTags;

    @Mock
    private IProfileRepository profileRepository;
    @Mock
    private IBusinessProfileService businessProfileService;

    @InjectMocks
    private ProfileService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        profile = new Profile("d376929f-c1b5-4e36-a10b-1e25105bdcf0", "095f8a9d-12f9-4cc1-ad50-b919bd24f419");

        stringTags = new ArrayList<>();
        stringTags.add(new StringTag("commercial transaction", "value"));
        doubleTags = new ArrayList<>();

        List<Todo> todos = new ArrayList<>();
        Todo todo = new Todo("simple todo", "simple type");
        todo.setTodoId("87734a77-bc57-44db-8be0-5b749ae37f16");
        todos.add(todo);
        profile.setTodos(todos);
        profile.setModuleReports(new ArrayList<>());
        profile.setStringTags(stringTags);
        profile.setDoubleTags(doubleTags);
        BusinessProfile businessProfile = new BusinessProfile();
        businessProfile.setBusinessName("TestBv");
        businessProfile.setOwnerIds(new ArrayList<>() {{ add("f6e21373-60c7-4c30-989f-28a746ce23c4"); }});
        profile.setBusinessProfile(businessProfile);
    }

    @Test
    public void getProfileAccountIdValid() {
        when(profileRepository.findTopByAccountId(anyString())).thenReturn(Optional.of(profile));

        Profile foundProfile = service.getProfileByAccountId(profile.getAccountId());

        Assert.assertEquals(profile, foundProfile);
    }

    @Test
    public void getProfileAccountIdNotFound() {
        when(profileRepository.findTopByAccountId(anyString())).thenReturn(Optional.empty());

        Profile foundProfile = service.getProfileByAccountId(profile.getAccountId());

        Assert.assertEquals(new Profile().getProfileId(), foundProfile.getProfileId());
    }

    @Test
    public void addModuleReportsValid() {
        Profile testProfile = profile;
        testProfile.getModuleReports().add(new ModuleReport("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46"));

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(profile));
        when(profileRepository.save(any(Profile.class))).thenReturn(testProfile);

        Profile updatedProfile = service.addModuleReports("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");

        verify(profileRepository, times(1)).save(any(Profile.class));
        Assert.assertFalse(updatedProfile.getModuleReports().isEmpty());
    }

    @Test(expected = ProfileNotFoundException.class)
    public void addModuleReportsNotFound() {
        when(profileRepository.findById(anyString())).thenReturn(Optional.empty());

        service.addModuleReports("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");
    }

    @Test
    public void deleteModuleReportValid() {
        Profile testProfile = profile;
        ModuleReport moduleReport = new ModuleReport("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");
        moduleReport.setModuleReportId("b1824801-003b-4298-8157-1d51f4e04c46");
        moduleReport.setProfile(profile);
        testProfile.getModuleReports().add(moduleReport);

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(testProfile));

        service.deleteModuleReport("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");

        verify(profileRepository, times(1)).save(any(Profile.class));
    }

    @Test(expected = ProfileNotFoundException.class)
    public void deleteModuleReportNotFound() {
        when(profileRepository.findById(anyString())).thenReturn(Optional.empty());

        service.deleteModuleReport("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");
    }

    @Test
    public void addTodoValid() {
        Profile testProfile = profile;
        Todo todo = new Todo("do something yeet", "do it yourself");
        todo.setTodoId("b1824801-003b-4298-8157-1d51f4e04c46");
        todo.setProfile(profile);
        testProfile.getTodos().add(new Todo("do something yeet", "do it yourself"));

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(profile));
        when(profileRepository.save(any(Profile.class))).thenReturn(testProfile);

        Profile updatedProfile = service.addTodo("b1824801-003b-4298-8157-1d51f4e04c46", "do something yeet", "do it yourself");

        verify(profileRepository, times(1)).save(any(Profile.class));
        Assert.assertFalse(updatedProfile.getTodos().isEmpty());
    }

    @Test(expected = ProfileNotFoundException.class)
    public void addTodoNotFound() {
        when(profileRepository.findById(anyString())).thenReturn(Optional.empty());

        service.addTodo("b1824801-003b-4298-8157-1d51f4e04c46", "do something yeet", "do it yourself");
    }

    @Test
    public void editTodoValid() {
        String previousTodoType = profile.getTodos().get(0).getTodoType();

        List<Todo> todos = new ArrayList<>();
        Todo todo = new Todo("simple todo", "do it yourself");
        todo.setTodoId("87734a77-bc57-44db-8be0-5b749ae37f16");
        todo.setProfile(profile);
        todos.add(todo);
        Profile testProfile = profile;
        testProfile.setTodos(todos);
        testProfile.setProfileId("854b07de-6de5-49ca-8632-6948396e1882");

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(profile));
        when(profileRepository.save(any(Profile.class))).thenReturn(testProfile);

        Profile updatedProfile = service.editTodo(profile.getProfileId(), todo.getTodoId(), todo.getTodoType(), todo.getAssignment(), false);

        verify(profileRepository, times(1)).save(any(Profile.class));
        Assert.assertNotEquals(updatedProfile.getTodos().get(0).getTodoType(), previousTodoType);
    }

    @Test
    public void deleteTodoValid() {
        Profile testProfile = profile;
        Todo todo = new Todo("do something yeet", "do it yourself");
        todo.setTodoId("b1824801-003b-4298-8157-1d51f4e04c46");
        todo.setProfile(profile);
        testProfile.getTodos().add(todo);

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(testProfile));

        service.deleteTodo("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");

        verify(profileRepository, times(1)).save(any(Profile.class));
    }

    @Test(expected = ProfileNotFoundException.class)
    public void deleteTodoNotFound() {
        when(profileRepository.findById(anyString())).thenReturn(Optional.empty());

        service.deleteTodo("b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46");
    }

    @Test
    public void deleteProfile() {
        when(profileRepository.findById(any())).thenReturn(Optional.of(profile));
        when(businessProfileService.deleteAccount(any(), any(), any())).thenReturn(new BusinessProfile());

        service.deleteProfile("b1824801-003b-4298-8157-1d51f4e04c46");

        verify(profileRepository).delete(any(Profile.class));
    }

    @Test
    public void addTagValid() {
        List<StringTag> newStringTags = new ArrayList<>();
        StringTag tag1 = new StringTag("commercial transaction", "B2B");
        tag1.setStringTagId("6253ac2b-55d0-4294-83dc-c0cfdcdb1222");
        newStringTags.add(tag1);
        StringTag tag2 = new StringTag("Business Model", "Hardware");
        tag2.setStringTagId("a6251de6-9287-43d4-afba-adf697ccb2a0");
        newStringTags.add(tag2);
        newStringTags.add(new StringTag("NewTag", "value"));
        Profile newProfile = profile;

        when(profileRepository.findById(anyString())).thenReturn(Optional.of(profile));
        when(profileRepository.save(any(Profile.class))).thenReturn(newProfile);

        String previousSize = String.valueOf(profile.getStringTags().size());
        Profile updateProfile = service.addTag(profile.getProfileId(), "NewTag", "value");

        verify(profileRepository).save(newProfile);
        Assert.assertNotEquals(previousSize, updateProfile.getStringTags().size());
    }

    @Test(expected = TagAlreadyExistsException.class)
    public void addTagAlreadyExists() {
        when(profileRepository.findById(anyString())).thenReturn(Optional.of(profile));

        service.addTag(profile.getProfileId(), "commercial transaction", "value");
    }
}
