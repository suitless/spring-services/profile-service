package nl.suitless.profileservice;

import nl.suitless.profileservice.Config.Invitations.InvitationConfig;
import nl.suitless.profileservice.Data.IBusinessInviteRepository;
import nl.suitless.profileservice.Data.IBusinessProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessInvite;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessInviteBody;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.BusinessProfileNotFoundException;
import nl.suitless.profileservice.Domain.Exceptions.InvitationException;
import nl.suitless.profileservice.Domain.Exceptions.InvitationNotFoundException;
import nl.suitless.profileservice.Services.Business.BusinessInviteService;
import nl.suitless.profileservice.Services.Interfaces.IAuthorizationService;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BusinessInviteServiceUnitTests {

    private BusinessProfile businessProfile;
    private BusinessInvite businessInvite;
    private Collection<GrantedAuthority> authorities;
    private Collection<GrantedAuthority> wrongAuthorities;

    @Mock
    private IBusinessProfileRepository businessProfileRepository;
    @Mock
    private IBusinessInviteRepository businessInviteRepository;
    @Mock
    private IAuthorizationService authService;
    @Mock
    private IBusinessProfileService businessProfileService;
    @Mock
    private InvitationConfig invitationConfig;
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private BusinessInviteService businessInviteService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("b1824801-003b-4298-8157-1d51f4e04c46");
        businessProfile = new BusinessProfile("TestBusiness",ownerIds);

        List<StringTag> tags = new ArrayList<>();
        StringTag tag1 = new StringTag("commercial transaction", "B2B");
        tag1.setStringTagId("6253ac2b-55d0-4294-83dc-c0cfdcdb1222");
        tags.add(tag1);
        StringTag tag2 = new StringTag("Business Model", "Hardware");
        tag2.setStringTagId("a6251de6-9287-43d4-afba-adf697ccb2a0");
        tags.add(tag2);
        StringTag tag3 = new StringTag("Business Entity", "Foundation");
        tag3.setStringTagId("e8ca2800-a5ac-4e6b-b61b-ffbdaa05f332");
        tags.add(tag3);
        businessProfile.setStringTags(tags);

        Profile profile = new Profile("bd73df8d-92b0-402a-ab81-220ece6f648b");
        profile.setRole(Role.FINANCIAL);
        businessProfile.getProfiles().add(profile);
        businessProfile.setBusinessProfileId("e5280bd7-a583-4a8c-8b17-47176bb04c3e");

        businessInvite = new BusinessInvite("test@test.com", new Date(), businessProfile.getBusinessProfileId());
        businessInvite.setBusinessInviteId("feef7ae2-be0a-43fe-ae60-4c453fdfe86b");

        authorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("admin"));

        wrongAuthorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) wrongAuthorities).add(0, new SimpleGrantedAuthority("user"));
    }

    @Test
    public void sendInviteValid() {
        when(businessProfileRepository.findById(any(String.class))).thenReturn(Optional.of(businessProfile));
        when(businessInviteRepository.save(any())).thenReturn(businessInvite);
        doNothing().when(authService).checkIsOwner(anyString(), any());
        when(invitationConfig.getBaseUrl()).thenReturn("test");
        when(invitationConfig.getEmailHttpCall()).thenReturn("test");
        when(restTemplate.postForEntity(anyString(), any(BusinessInviteBody.class), eq(String.class))).thenReturn(
                new ResponseEntity<>("", HttpStatus.OK));

        businessInviteService.sendInvite("20777dcb-e655-4f6d-917f-c3bf104b4280", "test@test.com", businessProfile);
    }

    @Test(expected = InvitationException.class)
    public void sendInviteInvitationException() {
        when(businessProfileRepository.findById(any(String.class))).thenReturn(Optional.of(businessProfile));
        when(businessInviteRepository.save(any())).thenReturn(businessInvite);
        doNothing().when(authService).checkIsOwner(anyString(), any());
        when(invitationConfig.getBaseUrl()).thenReturn("test");
        when(invitationConfig.getEmailHttpCall()).thenReturn("test");
        when(restTemplate.postForEntity(anyString(), any(BusinessInviteBody.class), eq(String.class))).thenReturn(
                new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR));

        businessInviteService.sendInvite("20777dcb-e655-4f6d-917f-c3bf104b4280", "test@test.com", businessProfile);
    }

    @Test(expected = BusinessProfileNotFoundException.class)
    public void sendInviteNotFound() {
        when(businessProfileRepository.findById(any(String.class))).thenReturn(Optional.empty());

        businessInviteService.sendInvite("20777dcb-e655-4f6d-917f-c3bf104b4280", "test@test.com", businessProfile);
    }

    @Test()
    public void acceptInviteValid() {
        when(businessInviteRepository.findById(anyString())).thenReturn(Optional.of(businessInvite));

        businessInviteService.acceptInvite("20777dcb-e655-4f6d-917f-c3bf104b4280",  businessInvite.getBusinessInviteId());

        verify(businessProfileService, times(1)).addAccount(anyString(), anyString(), any());
        verify(businessInviteRepository, times(1)).delete(any());
    }

    @Test(expected = InvitationNotFoundException.class)
    public void acceptInviteNotFound() {
        businessInviteService.acceptInvite("20777dcb-e655-4f6d-917f-c3bf104b4280",  businessInvite.getBusinessInviteId());
    }

    @Test()
    public void declineInviteValid() {
        when(businessInviteRepository.findById(anyString())).thenReturn(Optional.of(businessInvite));

        businessInviteService.declineInvite(businessInvite.getBusinessInviteId());

        verify(businessProfileService, never()).addAccount(anyString(), anyString(), any());
        verify(businessInviteRepository, times(1)).delete(any());
    }

    @Test(expected = InvitationNotFoundException.class)
    public void declineInviteNotFound() {
        businessInviteService.declineInvite(businessInvite.getBusinessInviteId());
    }
}
