package nl.suitless.profileservice;

import nl.suitless.profileservice.Data.IBusinessProfileRepository;
import nl.suitless.profileservice.Data.IProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.BusinessNameAlreadyExistsException;
import nl.suitless.profileservice.Domain.Exceptions.BusinessProfileNotFoundException;
import nl.suitless.profileservice.Domain.Exceptions.TagAlreadyExistsException;
import nl.suitless.profileservice.Services.Business.BusinessProfileService;
import nl.suitless.profileservice.Services.Interfaces.IAuthorizationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BusinessProfileServiceUnitTests {

    private BusinessProfile businessProfile;
    private Profile profile;
    private Collection<GrantedAuthority> authorities;
    private Collection<GrantedAuthority> wrongAuthorities;

    @Mock
    private IBusinessProfileRepository businessProfileRepository;
    @Mock //Needed for testing purposes do not remove
    private IProfileRepository profileRepository;
    @Mock //Needed for testing purposes do not remove
    private IAuthorizationService authorizationService;

    @InjectMocks
    private BusinessProfileService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("b1824801-003b-4298-8157-1d51f4e04c46");
        businessProfile = new BusinessProfile("TestBusiness",ownerIds);

        List<StringTag> tags = new ArrayList<>();
        StringTag tag1 = new StringTag("commercial transaction", "B2B");
        tag1.setStringTagId("6253ac2b-55d0-4294-83dc-c0cfdcdb1222");
        tags.add(tag1);
        StringTag tag2 = new StringTag("Business Model", "Hardware");
        tag2.setStringTagId("a6251de6-9287-43d4-afba-adf697ccb2a0");
        tags.add(tag2);
        StringTag tag3 = new StringTag("Business Entity", "Foundation");
        tag3.setStringTagId("e8ca2800-a5ac-4e6b-b61b-ffbdaa05f332");
        tags.add(tag3);
        businessProfile.setStringTags(tags);

        profile = new Profile("bd73df8d-92b0-402a-ab81-220ece6f648b");
        profile.setRole(Role.FINANCIAL);
        businessProfile.getProfiles().add(profile);
        businessProfile.setBusinessProfileId("e5280bd7-a583-4a8c-8b17-47176bb04c3e");
        businessProfile.setTemplateAssetId("e1b88249-d8fa-4903-bbd1-695c7350aa0c");

        authorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("admin"));

        wrongAuthorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) wrongAuthorities).add(0, new SimpleGrantedAuthority("user"));
    }

    @Test
    public void createBusinessProfileValid() {
        Profile profile = new Profile();
        profile.setProfileId("ba0f34ce-a448-43c9-ad2a-0481d3d04b27");
        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("65272d8e-784d-4aaf-bbb4-486ee775d7ad");
        List<StringTag> stringTags = new ArrayList<>();
        stringTags.add(new StringTag("commercial transaction", "B2B"));
        stringTags.add(new StringTag("Business Model", "Software"));
        stringTags.add(new StringTag("Business Entity", "Foundation"));
        List<DoubleTag> doubleTags = new ArrayList<>();
        doubleTags.add(new DoubleTag("Building Height", 20.0));
        BusinessProfile newBusinessProfile = new BusinessProfile("TestBv2", ownerIds);
        newBusinessProfile.setStringTags(stringTags);
        newBusinessProfile.setDoubleTags(doubleTags);
        newBusinessProfile.getProfiles().add(profile);
        newBusinessProfile.setTemplateAssetId("34136d22-a689-4d6b-83b4-5e5fd416af81");

        when(businessProfileRepository.save(any(BusinessProfile.class))).thenReturn(newBusinessProfile);

        BusinessProfile createdBusinessProfile = service.createBusinessProfile(
                profile.getAccountId(), newBusinessProfile.getBusinessName(), newBusinessProfile.getTemplateAssetId(),
                ownerIds, stringTags, doubleTags);

        verify(businessProfileRepository, times(1)).save(any(BusinessProfile.class));
        Assert.assertEquals(createdBusinessProfile.getBusinessName(), newBusinessProfile.getBusinessName());
    }

    @Test(expected = BusinessNameAlreadyExistsException.class)
    public void createBusinessProfileAlreadyExists() {
        Profile profile = new Profile();
        profile.setProfileId("ba0f34ce-a448-43c9-ad2a-0481d3d04b27");
        when(businessProfileRepository.getByBusinessName(businessProfile.getBusinessName())).thenReturn(Optional.of(businessProfile));

        service.createBusinessProfile(profile.getAccountId(), businessProfile.getBusinessName(),
                businessProfile.getTemplateAssetId(), businessProfile.getOwnerIds(), businessProfile.getStringTags(),
                businessProfile.getDoubleTags());
    }

    @Test
    public void UpdateBusinessProfileValid() {
        List<StringTag> stringTags = new ArrayList<>();
        stringTags.add(new StringTag("commercial transaction", "B2B"));
        stringTags.add(new StringTag("Business Model", "Software"));
        stringTags.add(new StringTag("Business Entity", "Foundation"));
        BusinessProfile newBusinessProfile = new BusinessProfile("testBv", businessProfile.getOwnerIds());
        newBusinessProfile.setStringTags(stringTags);

        when(businessProfileRepository.findById("b1824801-003b-4298-8157-1d51f4e04c46")).thenReturn(Optional.of(businessProfile));
        when(businessProfileRepository.save(any(BusinessProfile.class))).thenReturn(newBusinessProfile);

        BusinessProfile updatedBusinessProfile = service.updateBusinessProfile(
                "b1824801-003b-4298-8157-1d51f4e04c46", "b1824801-003b-4298-8157-1d51f4e04c46",
                businessProfile.getBusinessName(), businessProfile.getTemplateAssetId(), businessProfile.getOwnerIds(),
                businessProfile.getStringTags(), businessProfile.getDoubleTags());

        verify(businessProfileRepository, times(1)).save(any(BusinessProfile.class));
        Assert.assertNotEquals(updatedBusinessProfile.getBusinessName(), businessProfile.getBusinessName());
        Assert.assertEquals(updatedBusinessProfile.getStringTag("Business Model").getValue(), "Software");
    }

    @Test(expected = BusinessProfileNotFoundException.class)
    public void UpdateBusinessProfileNotFound() {
        when(businessProfileRepository.findById("b1824801-003b-4298-8157-1d51f4e04c46")).thenReturn(Optional.empty());

        service.updateBusinessProfile("b1824801-003b-4298-8157-1d51f4e04c46",
                "b1824801-003b-4298-8157-1d51f4e04c46", businessProfile.getBusinessName(), businessProfile.getTemplateAssetId(),
                businessProfile.getOwnerIds(), businessProfile.getStringTags(), businessProfile.getDoubleTags());
    }

    @Test
    public void getBusinessProfileById() {
        when(businessProfileRepository.findById(any(String.class))).thenReturn(Optional.of(businessProfile));

        BusinessProfile foundBusinessProfile = service.getBusinessProfileById("b1824801-003b-4298-8157-1d51f4e04c46");

        Assert.assertNotNull(foundBusinessProfile);
    }

    @Test(expected = BusinessProfileNotFoundException.class)
    public void getBusinessProfileByIdNotFound() {
        when(businessProfileRepository.findById(any(String.class))).thenReturn(Optional.empty());

        service.getBusinessProfileById("b1824801-003b-4298-8157-1d51f4e04c46");
    }

    @Test
    public void getAllBusinessProfilesValid() {
        when(businessProfileRepository.findAll()).thenReturn(anyIterable());

        List<BusinessProfile> foundBusinessProfiles = service.getAllBusinessProfiles(authorities);

        Assert.assertEquals(foundBusinessProfiles.size(), 0);
    }

    @Test
    public void deleteBusinessProfileValid() {
        when(businessProfileRepository.findById(anyString())).thenReturn(Optional.of(businessProfile));

        service.deleteBusinessProfile(authorities, "b1824801-003b-4298-8157-1d51f4e04c46");

        verify(businessProfileRepository, times(1)).delete(businessProfile);
    }

    @Test(expected = BusinessProfileNotFoundException.class)
    public void deleteBusinessProfileNotFound() {
        service.deleteBusinessProfile(authorities, "b1824801-003b-4298-8157-1d51f4e04c46");
    }

    @Test
    public void addTagValid() {
        List<StringTag> tags = new ArrayList<>();
        StringTag tag1 = new StringTag("commercial transaction", "B2B");
        tag1.setStringTagId("6253ac2b-55d0-4294-83dc-c0cfdcdb1222");
        tags.add(tag1);
        StringTag tag2 = new StringTag("Business Model", "Hardware");
        tag2.setStringTagId("a6251de6-9287-43d4-afba-adf697ccb2a0");
        tags.add(tag2);
        StringTag tag3 = new StringTag("Business Entity", "Foundation");
        tag3.setStringTagId("e8ca2800-a5ac-4e6b-b61b-ffbdaa05f332");
        tags.add(tag3);
        tags.add(new StringTag("Test", "value"));
        BusinessProfile newBusinessProfile = new BusinessProfile(
                businessProfile.getBusinessName(), businessProfile.getTemplateAssetId(), businessProfile.getOwnerIds(),
                tags, businessProfile.getDoubleTags(), businessProfile.getProfiles());


        when(businessProfileRepository.findById(anyString())).thenReturn(Optional.of(businessProfile));
        when(businessProfileRepository.save(any(BusinessProfile.class))).thenReturn(newBusinessProfile);

        BusinessProfile updateBusinessProfile = service.addTag(businessProfile.getBusinessProfileId(), "Test", "value");

        verify(businessProfileRepository, times(1)).findById(anyString());
        verify(businessProfileRepository, times(1)).save(any(BusinessProfile.class));
        Assert.assertNotEquals(updateBusinessProfile.getStringTags().size(), 3);
    }

    @Test(expected = TagAlreadyExistsException.class)
    public void addTagAlreadyExists() {
        when(businessProfileRepository.findById(anyString())).thenReturn(Optional.of(businessProfile));

        service.addTag(businessProfile.getBusinessProfileId(), "commercial transaction", "value");
    }

    @Test
    public void editTagValid() {
        BusinessProfile newBusinessProfile = new BusinessProfile(
                businessProfile.getBusinessName(), businessProfile.getTemplateAssetId(), businessProfile.getOwnerIds(),
                businessProfile.getStringTags(), businessProfile.getDoubleTags(), businessProfile.getProfiles());
        newBusinessProfile.getStringTag("Business Model").setValue("Software");

        when(businessProfileRepository.findById(anyString())).thenReturn(Optional.of(businessProfile));
        when(businessProfileRepository.save(any(BusinessProfile.class))).thenReturn(newBusinessProfile);

        service.editTag(businessProfile.getBusinessProfileId(), "Business Model", "Software");

        verify(businessProfileRepository, times(1)).findById(anyString());
        verify(businessProfileRepository, times(1)).save(any(BusinessProfile.class));
        Assert.assertEquals(businessProfile.getStringTag("Business Model").getValue(), "Software");
    }

    @Test
    public void deleteTagValid() {
        List<StringTag> tags = new ArrayList<>();
        tags.add(new StringTag("commercial transaction", "B2B"));
        tags.add(new StringTag("Business Model", "Hardware"));
        tags.add(new StringTag("Business Entity", "Foundation"));
        BusinessProfile newBusinessProfile = new BusinessProfile(
                businessProfile.getBusinessName(), businessProfile.getTemplateAssetId(), businessProfile.getOwnerIds(),
                tags, businessProfile.getDoubleTags(), businessProfile.getProfiles());
        newBusinessProfile.getStringTags().remove(0);

        when(businessProfileRepository.findById(anyString())).thenReturn(Optional.of(businessProfile));
        when(businessProfileRepository.save(any(BusinessProfile.class))).thenReturn(newBusinessProfile);

        service.deleteTag(businessProfile.getBusinessProfileId(), businessProfile.getStringTag("commercial transaction").getTagId());

        verify(businessProfileRepository, times(1)).findById(anyString());
        verify(businessProfileRepository, times(1)).save(any(BusinessProfile.class));
    }

    @Test
    public void checkAccountInBusinessValidTrue() {
        when(businessProfileRepository.getByBusinessName(anyString())).thenReturn(Optional.of(businessProfile));

        boolean result = service.checkAccountInBusiness(businessProfile.getBusinessName(), profile.getAccountId());

        verify(businessProfileRepository, times(1)).getByBusinessName(anyString());
        Assert.assertTrue(result);
    }

    @Test
    public void checkAccountInBusinessValidFalse() {
        when(businessProfileRepository.getByBusinessName(anyString())).thenReturn(Optional.of(businessProfile));

        boolean result = service.checkAccountInBusiness(businessProfile.getBusinessName(), "62ca51a9-f418-41e1-88cd-6f28c093c7b1");

        verify(businessProfileRepository, times(1)).getByBusinessName(anyString());
        Assert.assertFalse(result);
    }
}
