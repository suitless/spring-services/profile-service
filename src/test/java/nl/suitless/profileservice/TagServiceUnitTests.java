package nl.suitless.profileservice;

import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.ITagProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Services.TagService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceUnitTests {

    private Profile profile;
    private List<StringTag> stringTags;
    private List<DoubleTag> doubleTags;
    private static final double DELTA = 1e-5;

    public static class TagServiceImpl extends TagService {
        public ITagProfile addTag(ITagProfile tagProfile, String key, Object value) {
            return super.addTag(tagProfile, key, value);
        }

        public ITagProfile editTag(ITagProfile tagProfile, String key, Object value) {
            return super.editTag(tagProfile, key, value);
        }

        public ITagProfile deleteTag(ITagProfile tagProfile, String key) {
            return super.deleteTag(tagProfile, key);
        }
    };

    @InjectMocks
    private TagServiceImpl tagService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        profile = new Profile("d376929f-c1b5-4e36-a10b-1e25105bdcf0", "095f8a9d-12f9-4cc1-ad50-b919bd24f419");

        stringTags = new ArrayList<>();
        stringTags.add(new StringTag("commercial transaction", "value"));
        doubleTags = new ArrayList<>();
        doubleTags.add(new DoubleTag("currency", 1.00));

        profile.setStringTags(stringTags);
        profile.setDoubleTags(doubleTags);
    }

    @Test
    public void addStringTagValid() {
        Profile updatedProfile = (Profile) tagService.addTag(profile, "TestKey", "TestValue");

        Assert.assertEquals(2, updatedProfile.getStringTags().size());
    }

    @Test
    public void addDoubleTagValid() {
        Profile updatedProfile = (Profile) tagService.addTag(profile, "TestKey", 1.00);

        Assert.assertEquals(2, updatedProfile.getDoubleTags().size());
    }

    @Test
    public void addTagInvalidFalseValue() {
        Profile updatedProfile = (Profile) tagService.addTag(profile, "TestKey", profile);

        Assert.assertEquals(profile, updatedProfile);
    }

    @Test
    public void addTagInvalidFalseKey() {
        Profile updatedProfile = (Profile) tagService.editTag(profile, "FalseKey", "TestValue");

        Assert.assertEquals(profile, updatedProfile);
    }

    @Test
    public void editStringTagValid() {
        Profile updatedProfile = (Profile) tagService.editTag(profile, "commercial transaction", "TestValue");

        Assert.assertEquals(profile.getStringTags().size(), updatedProfile.getStringTags().size());
        Assert.assertEquals("TestValue", updatedProfile.getStringTags().get(0).getValue());
    }

    @Test
    public void editDoubleTagValid() {
        Profile updatedProfile = (Profile) tagService.editTag(profile, "currency", 2.00);

        Assert.assertEquals(profile.getDoubleTags().size(), updatedProfile.getDoubleTags().size());
        Assert.assertEquals(2.00, profile.getDoubleTags().get(0).getValue(), DELTA);
    }

    @Test
    public void editTagInvalidFalseValue() {
        Profile updatedProfile = (Profile) tagService.editTag(profile, "TestKey", profile);

        Assert.assertEquals(profile, updatedProfile);
    }

    @Test
    public void deleteTagValid() {
        Profile updatedProfile = (Profile) tagService.deleteTag(profile, "commercial transaction");

        Assert.assertEquals(0, updatedProfile.getStringTags().size());
    }

    @Test
    public void deleteDoubleTagValid() {
        Profile updatedProfile = (Profile) tagService.deleteTag(profile, "currency");

        Assert.assertEquals(0, updatedProfile.getDoubleTags().size());
    }
}
