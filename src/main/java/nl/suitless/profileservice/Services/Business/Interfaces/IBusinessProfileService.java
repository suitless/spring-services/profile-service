package nl.suitless.profileservice.Services.Business.Interfaces;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Entities.User;
import nl.suitless.profileservice.Domain.Exceptions.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

public interface IBusinessProfileService {
    /**
     * Creates a new business profile, a business type and also adds a profile to the business profile.
     * @param accountId the account id to get the profile you want to connect
     * @param businessName name of the business
     * @param templateAssetId the id of the template used for generating the report for the modules that the business contains
     * @param ownerIds a list of account ids that own the business
     * @param stringTags list of tags that contain a string value
     * @param doubleTags list of tags that contain a double value
     * @return created businessProfile
     * @throws BusinessNameAlreadyExistsException if the business name is already in use
     * @throws AccountAlreadyInABusinessException if the profile is already in a business
     * @throws ProfileNotFoundException if the given profile is not found
     */
    BusinessProfile createBusinessProfile(String accountId, String businessName, String templateAssetId,
                                          List<String> ownerIds, List<StringTag> stringTags, List<DoubleTag> doubleTags);

    /**
     * Update the business profile.
     * Only the business owner is allowed to do this.
     * @param accountId the id of the account you want to update the business with (only owners can update a business)
     * @param businessId the id of the business profile
     * @param businessName name of the business
     * @param templateAssetId the id of the template used for generating the report for the module that the business contains
     * @param ownerIds a list of account ids that own the business
     * @param stringTags list of tags that contain a string value and you want to update
     * @param doubleTags list of tags that contain a double value and you want to update
     * @return updated businessProfile
     * @throws UnauthorizedAccount if the given account id is not an owner id (no permission)
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     */
    BusinessProfile updateBusinessProfile(String accountId, String businessId, String businessName, String templateAssetId,
                                          List<String> ownerIds, List<StringTag> stringTags, List<DoubleTag> doubleTags);

    /**
     * Returns found business profile by id.
     * @param id of the business profile
     * @return found businessProfile
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     */
    BusinessProfile getBusinessProfileById(String id);

    /**
     * Get all business profiles.
     * Only Authorized Users are allowed to delete.
     * Look in the application.yml under authorization.roles for the allowed roles.
     * @param authorities of the user
     * @return all business profiles found
     * @throws UnauthorizedAccount if the user is not authorised
     */
    List<BusinessProfile> getAllBusinessProfiles(Collection<GrantedAuthority> authorities);

    /**
     * Get all users within a business profile.
     * Only the business owner is allowed to do this.
     * @param accountId of the user
     * @param id of the business profile
     * @return found users
     * @throws UnauthorizedAccount if the given account id is not an owner id (no permission)
     * @throws BusinessProfileNotFoundException if the profile with id is not found in the system.
     */
    List<User> getUsersFromBusinessProfile(String accountId, String id);

    /**
     * Deletes business profile in the database with the given id.
     * Only Authorized Users are allowed to delete.
     * Look in the application.yml under authorization.roles for the allowed roles.
     * @param authorities of the user
     * @param id of the profile  you wish to delete
     * @throws BusinessProfileNotFoundException if the profile with id is not found in the system.
     */
    void deleteBusinessProfile(Collection<GrantedAuthority> authorities, String id);

    /**
     * Add an optional tag.
     * @param businessId of the business
     * @param key of the tag
     * @param value of the tag
     * @return updated businessProfile with the added tag
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     * @throws TagAlreadyExistsException if the tag already exists in the business profile
     */
    BusinessProfile addTag(String businessId, String key, Object value);

    /**
     * Edit the given tag.
     * @param businessId of the business
     * @param key of the tag
     * @param value of the tag
     * @return updated businessProfile with the updated tag
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     * @throws TagNotFoundException if the tag could not be found
     */
    BusinessProfile editTag(String businessId, String key, Object value);

    /**
     * Delete an optional tag.
     * @param businessId of the business
     * @param key of the tag
     * @return updated businessProfile with the updated tag
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     * @throws TagNotFoundException if the tag could not be found
     */
    BusinessProfile deleteTag(String businessId, String key);

    /**
     * Adds an new account to the business.
     * Only allowed to be called by invite.
     * @param id of the business
     * @param accountId of the account
     * @param role of the account
     * @return the businessProfile with the new account
     * @throws BusinessProfileNotFoundException if the profile is not found by business name
     * @throws AccountAlreadyInABusinessException if the account is already in a business
     */
    BusinessProfile addAccount(String id, String accountId, Role role);

    /**
     * Deletes an account from the business.
     * Only the business owner is allowed to do this.
     * @param accountId of the user
     * @param id of the business
     * @param accountIdToDelete needs to be different from the owner account id
     * @return the businessProfile with the removed account
     * @throws BusinessProfileNotFoundException if the profile is not found by business name
     * @throws CantRemoveAccountFromBusinessProfileException if the account can't be removed because it's the only one left or an owner
     */
    BusinessProfile deleteAccount(String accountId, String id, String accountIdToDelete);

    /**
     * Checks if an account has a profile and is in the given business
     * @param businessName name of the business the profile needs to have
     * @param accountId id of the account
     * @return if the account is in the given business
     * @throws BusinessProfileNotFoundException if the profile is not found by business id
     */
    boolean checkAccountInBusiness(String businessName, String accountId);
}
