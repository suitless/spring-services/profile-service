package nl.suitless.profileservice.Services.Business;

import nl.suitless.profileservice.Config.Invitations.InvitationConfig;
import nl.suitless.profileservice.Data.IBusinessInviteRepository;
import nl.suitless.profileservice.Data.IBusinessProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessInvite;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessInviteBody;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Exceptions.BusinessProfileNotFoundException;
import nl.suitless.profileservice.Domain.Exceptions.InvitationException;
import nl.suitless.profileservice.Domain.Exceptions.InvitationNotFoundException;
import nl.suitless.profileservice.Services.Interfaces.IAuthorizationService;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessInviteService;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.joda.time.DateTime;
import java.util.Date;

@Service
public class BusinessInviteService implements IBusinessInviteService {
    private IBusinessInviteRepository businessInviteRepository;
    private IBusinessProfileRepository businessProfileRepository;
    private IAuthorizationService authService;
    private IBusinessProfileService businessProfileService;
    private InvitationConfig invitationConfig;
    private RestTemplate restTemplate;

    public BusinessInviteService(IBusinessInviteRepository businessInviteRepository,
                                 IBusinessProfileRepository businessProfileRepository,
                                 IAuthorizationService authService,
                                 IBusinessProfileService businessProfileService,
                                 InvitationConfig invitationConfig, RestTemplate restTemplate) {
        this.businessInviteRepository = businessInviteRepository;
        this.businessProfileRepository = businessProfileRepository;
        this.authService = authService;
        this.businessProfileService = businessProfileService;
        this.invitationConfig = invitationConfig;
        this.restTemplate = restTemplate;
    }

    @Override
    public BusinessProfile getBusinessProfileByInviteId(String inviteId) {
        BusinessInvite businessInvite = businessInviteRepository.findById(inviteId).orElseThrow(() ->
                new InvitationNotFoundException("The business invitation could not be found."));

        return getBusinessProfileByIdIfPresent(businessInvite.getBusinessId());
    }

    @Override
    public BusinessInvite sendInvite(String accountId, String email, BusinessProfile businessProfile) {
        this.authService.checkIsOwner(accountId, businessProfile);
        var foundBusinessProfile = getBusinessProfileByIdIfPresent(businessProfile.getBusinessProfileId());
        // Check if the business invite already exists, no need to create 2
        var foundBusinessInvite = businessInviteRepository.getByEmailAndBusinessId(email, foundBusinessProfile.getBusinessProfileId());

        BusinessInvite businessInvite;
        if (foundBusinessInvite.isEmpty()) {
            Date expirationDate = DateTime.now().plusDays(invitationConfig.getDaysValid()).toDate();
            businessInvite = businessInviteRepository.save(new BusinessInvite(email, expirationDate, businessProfile.getBusinessProfileId()));
        } else {
            businessInvite = foundBusinessInvite.get();
        }

        BusinessInviteBody body = new BusinessInviteBody(email,
                invitationConfig.getBaseUrl() + "/" + businessInvite.getBusinessInviteId(),
                businessProfile.getBusinessName());
        ResponseEntity<String> sendInviteEmail = restTemplate.postForEntity(
                invitationConfig.getEmailHttpCall(),
                body, String.class);

        if (sendInviteEmail.getStatusCode().isError()) {
            businessInviteRepository.delete(businessInvite);
            throw new InvitationException("Something went wrong when sending the invitation email");
        }

        return businessInvite;
    }

    @Override
    public void acceptInvite(String accountId, String businessInviteId) {
        BusinessInvite businessInvite = getBusinessInviteByIdIfPresent(businessInviteId);

        businessProfileService.addAccount(businessInvite.getBusinessId(), accountId, Role.ALL);

        businessInviteRepository.delete(businessInvite);
    }

    @Override
    public void declineInvite(String businessInviteId) {
        BusinessInvite businessInvite = getBusinessInviteByIdIfPresent(businessInviteId);
        businessInviteRepository.delete(businessInvite);
    }

    //#region Generic methods

    private BusinessProfile getBusinessProfileByIdIfPresent(String id) {
        return businessProfileRepository.findById(id).orElseThrow(() ->
                new BusinessProfileNotFoundException("The business with id: " + id + " can't be found."));
    }

    private BusinessInvite getBusinessInviteByIdIfPresent(String id) {
        return businessInviteRepository.findById(id).orElseThrow(() ->
                new InvitationNotFoundException("Could not find the invitation, are you using the correct account?"));
    }

    //#endregion
}
