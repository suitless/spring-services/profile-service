package nl.suitless.profileservice.Services.Business;

import nl.suitless.profileservice.Data.IBusinessProfileRepository;
import nl.suitless.profileservice.Data.IProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Entities.User;
import nl.suitless.profileservice.Domain.Exceptions.*;
import nl.suitless.profileservice.Services.Interfaces.IAuthorizationService;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import nl.suitless.profileservice.Services.TagService;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.*;

@Service
public class BusinessProfileService extends TagService implements IBusinessProfileService {
    private IBusinessProfileRepository businessProfileRepository;
    private IProfileRepository profileRepository;
    private IAuthorizationService authService;
    private KeycloakRestTemplate restTemplate;
    private String keycloakServerUrl;
    private String realm;

    @Autowired
    public BusinessProfileService(IBusinessProfileRepository businessProfileRepository, IProfileRepository profileRepository,
                                  IAuthorizationService authService, KeycloakRestTemplate restTemplate,
                                  @Value("${keycloak.auth-server-url}") String keycloakServerUrl,
                                  @Value("${keycloak.realm}") String realm) {
        this.businessProfileRepository = businessProfileRepository;
        this.profileRepository = profileRepository;
        this.authService = authService;
        this.restTemplate = restTemplate;
        this.keycloakServerUrl = keycloakServerUrl;
        this.realm = realm;
    }

    @Override
    public BusinessProfile createBusinessProfile(String accountId, String businessName, String templateAssetId,
                                                 List<String> ownerIds, List<StringTag> stringTags, List<DoubleTag> doubleTags) {
        checkIfBusinessAlreadyExists(businessName);
        if(ownerIds == null) {
            ownerIds = new ArrayList<>();
            ownerIds.add(accountId);
        }

        for (String id : ownerIds) {
            checkAccountAlreadyInABusiness(id);
        }

        BusinessProfile businessProfile = new BusinessProfile(businessName, ownerIds);
        businessProfile.setTemplateAssetId(templateAssetId);
        businessProfile.setStringTags(stringTags);
        businessProfile.setDoubleTags(doubleTags);

        return businessProfileRepository.save(businessProfile);
    }

    @Override
    public BusinessProfile updateBusinessProfile(String accountId, String businessId, String businessName, String templateAssetId,
                                                 List<String> ownerIds, List<StringTag> stringTags, List<DoubleTag> doubleTags) {
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(businessId);
        this.authService.checkIsOwner(accountId, businessProfile);

        businessProfile.setBusinessName(businessName);
        businessProfile.setTemplateAssetId(templateAssetId);
        businessProfile.setOwnerIds(ownerIds);
        for (StringTag tag : stringTags) {
            StringTag foundTag = businessProfile.getStringTag(tag.getKey());
            if (foundTag != null) {
                foundTag.setValue(tag.getValue());
            }
        }

        for (DoubleTag tag : doubleTags) {
            DoubleTag foundTag = businessProfile.getDoubleTag(tag.getKey());
            if (foundTag != null) {
                foundTag.setValue(tag.getValue());
            }
        }

        return businessProfileRepository.save(businessProfile);
    }

    @Override
    public BusinessProfile getBusinessProfileById(String id) {
        return getBusinessProfileByIdIfPresent(id);
    }

    @Override
    public List<BusinessProfile> getAllBusinessProfiles(Collection<GrantedAuthority> authorities) {
        this.authService.check(authorities);

        return (List<BusinessProfile>) businessProfileRepository.findAll();
    }

    @Override
    public List<User> getUsersFromBusinessProfile(String accountId, String id) {
        BusinessProfile businessProfile = getBusinessProfileById(id);
        this.authService.checkIsOwner(accountId, businessProfile);

        List<User> users = new ArrayList<>();
        // The Keycloak-API has no functionality to retrieve bulk users from a list of id's
        // TODO: add a Keycloak ServiceProvider to add the functionality to Keycloak
        for (Profile profile : businessProfile.getProfiles()) {
            User user = restTemplate.getForEntity(
                    URI.create(keycloakServerUrl + "/admin/realms/" + realm + "/users/" + profile.getAccountId()),
                    User.class).getBody();
            users.add(user);
        }

        return users;
    }

    @Override
    public void deleteBusinessProfile(Collection<GrantedAuthority> authorities, String id) {
        this.authService.check(authorities);
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(id);

        businessProfileRepository.delete(businessProfile);
    }

    @Override
    public BusinessProfile addTag(String businessId, String key, Object value) {
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(businessId);

        return businessProfileRepository.save((BusinessProfile) super.addTag(businessProfile, key, value));
    }

    @Override
    public BusinessProfile editTag(String businessId, String key, Object value) {
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(businessId);
        businessProfile = (BusinessProfile) super.editTag(businessProfile, key, value);

        return businessProfileRepository.save(businessProfile);
    }

    @Override
    public BusinessProfile deleteTag(String businessId, String key) {
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(businessId);

        businessProfile = (BusinessProfile) super.deleteTag(businessProfile, key);
        return businessProfileRepository.save(businessProfile);
    }

    @Override
    public BusinessProfile addAccount(String id, String accountId, Role role) {
        checkAccountAlreadyInABusiness(accountId);
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(id);

        Profile profile = getProfileByAccountIdIfPresent(accountId);
        profile.setRole(role);
        profile.setBusinessProfile(businessProfile);

        businessProfile.getProfiles().add(profile);

        return businessProfileRepository.save(businessProfile);
    }

    @Override
    public BusinessProfile deleteAccount(String accountId, String id, String accountIdToDelete) {
        BusinessProfile businessProfile = getBusinessProfileByIdIfPresent(id);
        this.authService.checkIsOwner(accountId, businessProfile);

        Optional<Profile> foundProfile = businessProfile.getProfiles().stream().filter((account) ->
                account.getAccountId().equals(accountIdToDelete)).findFirst();
        if (foundProfile.isPresent() && businessProfile.getProfiles().size() > 1) {
            Profile profile = foundProfile.get();
            if (accountIsLastOwner(profile, businessProfile)) {
                profile.setBusinessProfile(null);
                profileRepository.save(profile);
                businessProfile.getProfiles().remove(profile);
                return businessProfileRepository.save(businessProfile);
            }
        }
        throw new CantRemoveAccountFromBusinessProfileException(
                "The account with id: " + accountId + " is not found or is the last one so it can't be removed.");
    }

    private boolean accountIsLastOwner(Profile profile, BusinessProfile businessProfile) {
        return businessProfile.getOwnerIds().contains(profile.getAccountId()) &&
                businessProfile.getOwnerIds().size() <= 1;
    }

    @Override
    public boolean checkAccountInBusiness(String businessName, String accountId) {
        BusinessProfile businessProfile = getBusinessProfileByNameIfPresent(businessName);

        for (Profile profile : businessProfile.getProfiles()) {
            if(profile.getAccountId().equals(accountId)) {
                return true;
            }
        }
        return false;
    }

    //#region Generic methods

    private void checkIfBusinessAlreadyExists(String businessName) {
        Optional<BusinessProfile> foundBusinessProfile = businessProfileRepository.getByBusinessName(businessName);
        if (foundBusinessProfile.isPresent()) {
            throw new BusinessNameAlreadyExistsException("The business with name: " + businessName + " already exists.");
        }
    }

    private BusinessProfile getBusinessProfileByIdIfPresent(String id) {
        return businessProfileRepository.findById(id).orElseThrow(() ->
                new BusinessProfileNotFoundException("The business with id: " + id + " can't be found."));
    }

    private BusinessProfile getBusinessProfileByNameIfPresent(String businessName) {
        return businessProfileRepository.getByBusinessName(businessName).orElseThrow(() ->
                new BusinessProfileNotFoundException("The business with the name: " + businessName + " not found"));
    }

    private Profile getProfileByAccountIdIfPresent(String accountId) {
        return profileRepository.findTopByAccountId(accountId).orElse(new Profile(accountId));
    }

    private void checkAccountAlreadyInABusiness(String accountId) {
        Optional<Profile> foundProfile = profileRepository.findTopByAccountId(accountId);
        if(foundProfile.isPresent()) {
            if(foundProfile.get().getBusinessProfile() != null) {
                throw new AccountAlreadyInABusinessException(
                        "The account is already in a business. You can only delete or edit an existing business.");
            }
        }
    }

    //#endregion
}
