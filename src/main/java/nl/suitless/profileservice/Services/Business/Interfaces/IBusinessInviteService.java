package nl.suitless.profileservice.Services.Business.Interfaces;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessInvite;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Exceptions.*;

public interface IBusinessInviteService {
    /**
     * Returns found business by invite id.
     * @param inviteId of the business invite
     * @return found business
     * @throws InvitationNotFoundException if the business invite could not be found
     * @throws BusinessProfileNotFoundException if the business could not be found
     */
    public BusinessProfile getBusinessProfileByInviteId(String inviteId);

    /**
     * Sends an email invite to the given address to join a business.
     * Only a business owner is allowed to send invites.
     * @param accountId of the account that wants to send the invite
     * @param email of the user to join the business
     * @param businessProfile for the user to join
     * @return created invite
     * @throws UnauthorizedAccount if the given account is not an owner (no permission)
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     * @throws InvitationException if there is something wrong with sending the email
     */
    public BusinessInvite sendInvite(String accountId, String email, BusinessProfile businessProfile);

    /**
     * Accept the invitation: join the given business and delete the invitation from the database.
     * @param accountId of the account that wants to join the business
     * @param businessInviteId of the business invite
     * @throws BusinessProfileNotFoundException if the business profile could not be found by id
     * @throws InvitationNotFoundException if the invitation could not be found
     */
    public void acceptInvite(String accountId, String businessInviteId);

    /**
     * Decline the invitation: not joining a business and delete the invitation from the database.
     * @param businessInviteId of the business invite
     * @throws InvitationNotFoundException if the invitation could not be found
     */
    public void declineInvite(String businessInviteId);
}
