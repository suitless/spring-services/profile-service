package nl.suitless.profileservice.Services.Business;

import nl.suitless.profileservice.Data.IBusinessInviteRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessInvite;
import org.joda.time.DateTime;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BusinessInviteTask {
    private final IBusinessInviteRepository businessInviteRepository;

    public BusinessInviteTask(IBusinessInviteRepository businessInviteRepository) {
        this.businessInviteRepository = businessInviteRepository;
    }

    @Scheduled(cron = "0 0 1 * * ?", zone = "Europe/Amsterdam")
    private void checkTokenExpired() {
        System.out.println("Cleaning up the invite database!! Shvoooooooooooooooooo");

        Iterable<BusinessInvite> businessInvites = businessInviteRepository.findAll();
        businessInvites.forEach(invite -> {
            if (DateTime.now().isAfter(new DateTime(invite.getExpirationDate()))) {
                businessInviteRepository.delete(invite);
            }
        });
    }
}
