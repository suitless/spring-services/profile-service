package nl.suitless.profileservice.Services;

import nl.suitless.profileservice.Config.RoleManagement.RoleManagementConfig;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Exceptions.UnauthorizedAccount;
import nl.suitless.profileservice.Services.Interfaces.IAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AuthorizationService implements IAuthorizationService {
    private RoleManagementConfig roleManagementConfig;

    @Autowired
    public AuthorizationService(RoleManagementConfig roleManagementConfig) {
        this.roleManagementConfig = roleManagementConfig;
    }

    @Override
    public void check(Collection<GrantedAuthority> authorities) {
        boolean isAuthorized = false;

        if(authorities != null && !authorities.isEmpty()) {
            for (GrantedAuthority authority: authorities) {
                for (String role: roleManagementConfig.getRoles()) {
                    if(authority.getAuthority().contains(role)) {
                        isAuthorized = true;
                        break;
                    }
                }
            }
        }

        if(!isAuthorized) {
            throw new UnauthorizedAccount("You are not allowed to retrieve this.");
        }
    }

    @Override
    public void checkIsOwner(String accountId, BusinessProfile businessProfile) {
        if(!businessProfile.getOwnerIds().contains(accountId)){
            throw new UnauthorizedAccount("You are not allowed to retrieve this.");
        }
    }
}
