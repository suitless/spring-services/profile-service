package nl.suitless.profileservice.Services;

import nl.suitless.profileservice.Domain.Entities.Tags.ITagProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.ITag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.TagAlreadyExistsException;

import java.util.Optional;
import java.util.List;

public abstract class TagService {
    protected ITagProfile addTag(ITagProfile tagProfile, String key, Object value) {
        if((!tagProfile.getStringTags().isEmpty() && tagProfile.getStringTags().stream().anyMatch(
                tag -> tag.getKey().equals(key))) ||
                (!tagProfile.getDoubleTags().isEmpty() && tagProfile.getDoubleTags().stream().anyMatch(
                        tag -> tag.getKey().equals(key)))) {
            throw new TagAlreadyExistsException("The tag with key: " + key + " already exists.");
        }

        if(value instanceof String) {
            tagProfile.getStringTags().add(new StringTag(key, (String) value));
        } else if(value instanceof Double) {
            tagProfile.getDoubleTags().add(new DoubleTag(key, (double) value));
        }

        return tagProfile;
    }

    protected ITagProfile editTag(ITagProfile tagProfile, String key, Object value) {
        if(value instanceof String) {
            List<ITag> filteredTags = filterTags((List<ITag>)(List<?>)tagProfile.getStringTags(), key, value);
            tagProfile.setStringTags((List<StringTag>)(List<?>) filteredTags);
        } else if(value instanceof Double) {
            List<ITag> filteredTags = filterTags((List<ITag>)(List<?>)tagProfile.getDoubleTags(), key, value);
            tagProfile.setDoubleTags((List<DoubleTag>)(List<?>) filteredTags);
        }

        return tagProfile;
    }

    private List<ITag> filterTags(List<ITag> tags, String key, Object value) {
        for(ITag tag: tags) {
            if(tag.getKey().equals(key)) {
                tag.setValue(value);
            }
        }

        return tags;
    }

    protected ITagProfile deleteTag(ITagProfile tagProfile, String key) {
        Optional<StringTag> foundStringTag = tagProfile.getStringTags().stream()
                .filter(tag -> tag.getKey().equals(key)).findFirst();
        if(!foundStringTag.isPresent()) {
            Optional<DoubleTag> foundDoubleTag = tagProfile.getDoubleTags().stream()
                    .filter(tag -> tag.getKey().equals(key)).findFirst();

            foundDoubleTag.ifPresent(doubleTag -> tagProfile.getDoubleTags().remove(doubleTag));
            return tagProfile;
        }

        tagProfile.getStringTags().remove(foundStringTag.get());
        return tagProfile;
    }
}
