package nl.suitless.profileservice.Services.Interfaces;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Exceptions.UnauthorizedAccount;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public interface IAuthorizationService {
    /**
     * Check if the user has the correct authorization to get certain data
     * @param authorities is a collection of granted authorities in the form of a String
     * @throws UnauthorizedAccount if the user is not authorised
     */
    void check(Collection<GrantedAuthority> authorities);

    /**
     * Check if the user is business owner
     * @param accountId of the account that needs to be checked if business owner
     * @param businessProfile the business profile that needs to be checked
     * @throws UnauthorizedAccount if the user is not the business owner
     */
    void checkIsOwner(String accountId, BusinessProfile businessProfile);
}
