package nl.suitless.profileservice.Services.Interfaces;

import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Exceptions.*;

import java.util.List;

public interface IProfileService {
    /**
     * Get Profile by account id.
     * @param accountId the id of the account
     * @return found profile
     */
    Profile getProfileByAccountId(String accountId);

    /**
     * Creates a new profile and a new business profile.
     * @param accountId the account id of the profile
     * @param role of the new account
     * @param businessName of the business
     * @param templateAssetId the id of the template used for generating the report for the modules that the business contains
     * @param ownerIds a list of account ids that own the business
     * @param amountOfEmployees amount of employees the business contains
     * @param businessStringTags list of tags that contain a string value
     * @param businessDoubleTags list of tags that contain a double value
     * @param stringTags list of tags that contain a string value
     * @param doubleTags list of tags that contain a double value
     * @return created profile with legal profile and new business profile
     */
    Profile createProfile(String accountId, Role role, String businessName, String templateAssetId, List<String> ownerIds,
                          int amountOfEmployees, List<StringTag> businessStringTags, List<DoubleTag> businessDoubleTags,
                          List<StringTag> stringTags, List<DoubleTag> doubleTags);

    /**
     * Create a new profile with an existing business profile.
     * @param accountId the account id of the profile
     * @param role of the new profile
     * @param businessId id of the existing business
     * @param stringTags list of tags that contain a string value
     * @param doubleTags list of tags that contain a double value
     * @return created profile with legal profile and business profile
     * @throws AccountAlreadyInABusinessException if the account is already in a business
     */
    Profile createProfile(String accountId, Role role, String businessId, List<StringTag> stringTags,
                          List<DoubleTag> doubleTags);

    /**
     * Create a new profile.
     * @param accountId of the profile
     * @param role of the new profile
     * @param stringTags list of tags that contain a string value
     * @param doubleTags list of tags that contain a double value
     * @return created profile with a new legal profile
     * @throws AccountAlreadyInABusinessException if the account is already in a business
     */
    Profile createProfile(String accountId, Role role, List<StringTag> stringTags, List<DoubleTag> doubleTags);

    /**
     * Updates profile with new role.
     * @param profileId of the profile
     * @param role the new role
     * @return updated profile
     * @throws ProfileNotFoundException if the profile could not be found
     */
    Profile updateProfile(String profileId, Role role);

    /**
     * Deletes the profile and from the business profile.
     * @param profileId of the profile
     * @throws ProfileNotFoundException if the profile could not be found
     */
    void deleteProfile(String profileId);

    /**
     * Creates a module report and links it to the profile in the database with the given input.
     * @param profileId of the profile
     * @param moduleId of the module
     * @param reportId of the report from the module
     * @return the updated profile with the new module report
     * @throws ProfileNotFoundException if the profile could not be found
     */
    Profile addModuleReports(String profileId, String moduleId, String reportId);

    /**
     * Deletes a module report from a profile.
     * @param profileId of the profile
     * @param moduleReportId of the module report
     * @throws ProfileNotFoundException if the profile could not be found
     * @throws ModuleReportNotFoundException if the module report could not be found
     */
    void deleteModuleReport(String profileId, String moduleReportId);

    /**
     * Adds a to do and links it to the profile in the database.
     * @param profileId of the profile
     * @param todoType the to-do type
     * @param assignment the to-do you want to add
     * @return the updated profile with the new to do
     * @throws ProfileNotFoundException if the profile could not be found
     */
    Profile addTodo(String profileId, String todoType, String assignment);

    /**
     * Updates an to do from a profile.
     * @param profileId the id of the profile
     * @param todoId of the to-do
     * @param todoType of the to-do
     * @param assignment the to-do contains
     * @param state of the assignment
     * @return the profile with the updated to-do
     * @throws TodoNotFoundException if the to-do could not be found
     */
    Profile editTodo(String profileId, String todoId, String todoType, String assignment, Boolean state);

    /**
     * Deletes to do from a profile.
     * @param profileId the id of the profile
     * @param todoId the of the to-do
     * @throws LegalProfileNotFoundException if the profile could not be found
     * @throws TodoNotFoundException if the to-do could not be found
     */
    void deleteTodo(String profileId, String todoId);

    /**
     * Add an optional tag
     * @param profileId of the profile
     * @param key of the tag
     * @param value of the tag
     * @return updated profile with the added tag
     * @throws ProfileNotFoundException if the profile could not be found
     * @throws TagAlreadyExistsException if the tag already exists in the profile
     */
    Profile addTag(String profileId, String key, Object value);

    /**
     * Edit the given tag
     * @param profileId of the profile
     * @param key of the tag
     * @param value of the tag
     * @return the profile with the updated tag
     * @throws ProfileNotFoundException if the profile could not be found
     * @throws TagNotFoundException if the tag could not be found
     */
    Profile editTag(String profileId, String key, Object value);

    /**
     * Delete an optional tag
     * @param profileId of the profile
     * @param key of the tag you want to delete
     * @return the profile with the deleted tag
     * @throws ProfileNotFoundException if the profile could not be found
     * @throws TagNotFoundException if the tag could not be found
     */
    Profile deleteTag(String profileId, String key);
}
