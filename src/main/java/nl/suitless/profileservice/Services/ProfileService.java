package nl.suitless.profileservice.Services;

import nl.suitless.profileservice.Data.IProfileRepository;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.ModuleReport;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Domain.Entities.Profile.Todo;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.*;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import nl.suitless.profileservice.Services.Interfaces.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfileService extends TagService implements IProfileService {
    private IProfileRepository profileRepository;
    private IBusinessProfileService businessProfileService;

    @Autowired
    public ProfileService(IProfileRepository profileRepository, IBusinessProfileService businessProfileService) {
        this.profileRepository = profileRepository;
        this.businessProfileService = businessProfileService;
    }

    @Override
    public Profile getProfileByAccountId(String accountId) {
        Optional<Profile> profile = profileRepository.findTopByAccountId(accountId);
        return profile.orElseGet(Profile::new);
    }

    @Override
    public Profile createProfile(String accountId, Role role, String businessName, String templateAssetId,
                                 List<String> ownerIds, int amountOfEmployees, List<StringTag> businessStringTags,
                                 List<DoubleTag> businessDoubleTags, List<StringTag> stringTags, List<DoubleTag> doubleTags) {
        Profile profile = new Profile(accountId);
        profile.setRole(role);
        BusinessProfile businessProfile = businessProfileService.createBusinessProfile(
                accountId, businessName, templateAssetId, ownerIds, businessStringTags, businessDoubleTags);

        profile.setBusinessProfile(businessProfile);
        profile = addTagsIfNotEmpty(profile, stringTags, doubleTags);

        return profileRepository.save(profile);
    }

    @Override
    public Profile createProfile(String accountId, Role role, String businessId, List<StringTag> stringTags,
                                 List<DoubleTag> doubleTags) {
        checkProfileByAccountId(accountId);

        BusinessProfile foundBusinessProfile = businessProfileService.getBusinessProfileById(businessId);

        Profile profile = new Profile(accountId);
        profile.setRole(role);
        profile = addTagsIfNotEmpty(profile, stringTags, doubleTags);
        profile.setBusinessProfile(foundBusinessProfile);

        return profileRepository.save(profile);
    }

    @Override
    public Profile createProfile(String accountId, Role role, List<StringTag> stringTags, List<DoubleTag> doubleTags) {
        checkProfileByAccountId(accountId);

        Profile profile = new Profile(accountId);
        profile.setRole(role);
        profile = addTagsIfNotEmpty(profile, stringTags, doubleTags);

        return profileRepository.save(profile);
    }

    @Override
    public Profile updateProfile(String profileId, Role role) {
        Profile profile = getProfileByIdIfPresent(profileId);
        profile.setRole(role);

        return profileRepository.save(profile);
    }

    private Profile addTagsIfNotEmpty(Profile profile, List<StringTag> stringTags, List<DoubleTag> doubleTags) {
        if(!stringTags.isEmpty()) {
            profile.setStringTags(stringTags);
        }
        if(!doubleTags.isEmpty()) {
            profile.setDoubleTags(doubleTags);
        }

        return profile;
    }

    @Override
    public void deleteProfile(String profileId) {
        Profile profile = getProfileByIdIfPresent(profileId);
        businessProfileService.deleteAccount(
                profile.getBusinessProfile().getOwnerIds().get(0), profile.getBusinessProfile().getBusinessProfileId(),
                profile.getAccountId());

        profileRepository.delete(profile);
    }

    @Override
    public Profile addModuleReports(String profileId, String moduleId, String reportId) {
        Profile profile = getProfileByIdIfPresent(profileId);
        ModuleReport moduleReport = new ModuleReport(moduleId, reportId);
        moduleReport.setProfile(profile);
        profile.getModuleReports().add(moduleReport);

        return profileRepository.save(profile);
    }

    @Override
    public void deleteModuleReport(String profileId, String moduleReportId) {
        Profile profile = getProfileByIdIfPresent(profileId);
        profile.getModuleReports().remove(getModuleReportIfPresent(profile, moduleReportId));

        profileRepository.save(profile);
    }

    @Override
    public Profile addTodo(String profileId, String todoType, String assignment) {
        Profile profile = getProfileByIdIfPresent(profileId);
        Todo todo = new Todo(assignment, todoType);
        todo.setProfile(profile);
        profile.getTodos().add(todo);

        return profileRepository.save(profile);
    }

    @Override
    public Profile editTodo(String profileId, String todoId, String todoType, String assignment, Boolean state) {
        Profile profile = getProfileByIdIfPresent(profileId);

        List<Todo> todos = profile.getTodos();
        for (Todo todo: todos) {
            if(todo.getTodoId().equals(todoId)) {
                todo.setTodoType(todoType);
                todo.setAssignment(assignment);
                todo.setState(state);
            }
        }

        profile.setTodos(todos);
        return profileRepository.save(profile);
    }

    @Override
    public void deleteTodo(String profileId, String todoId) {
        Profile profile = getProfileByIdIfPresent(profileId);
        profile.getTodos().remove(getTodoIfPresent(profile, todoId));

        profileRepository.save(profile);
    }

    @Override
    public Profile addTag(String profileId, String key, Object value) {
        Profile profile = getProfileByIdIfPresent(profileId);

        return profileRepository.save((Profile) super.addTag(profile, key, value));
    }

    @Override
    public Profile editTag(String profileId, String key, Object value) {
        Profile profile = getProfileByIdIfPresent(profileId);
        profile = (Profile) super.editTag(profile, key, value);

        return profileRepository.save(profile);
    }

    @Override
    public Profile deleteTag(String profileId, String key) {
        Profile profile = getProfileByIdIfPresent(profileId);
        profile = (Profile) super.deleteTag(profile, key);

        return profileRepository.save(profile);
    }

    //#region Generic methods

    private void checkProfileByAccountId(String accountId) {
        if(profileRepository.findTopByAccountId(accountId).isPresent()) {
            throw new AccountAlreadyInABusinessException("The account is already in a business. You can only delete or edit an existing business.");
        }
    }

    private Profile getProfileByIdIfPresent(String id) {
        return profileRepository.findById(id).orElseThrow(() ->
            new ProfileNotFoundException("Profile with id: " + id + " not found")
        );
    }

    private ModuleReport getModuleReportIfPresent(Profile profile, String moduleReportId) {
        return profile.getModuleReports().stream().filter((report) ->
                report.getModuleReportId().equals(moduleReportId)).findFirst().orElseThrow(() ->
                new ModuleReportNotFoundException(
                        "Module report with id: " + moduleReportId + " in the legal profile with id:" + profile.getProfileId() + " not found."));
    }

    private Todo getTodoIfPresent(Profile profile, String todoId) {
        return profile.getTodos().stream().filter((assignment) ->
                assignment.getTodoId().equals(todoId)).findFirst().orElseThrow(() ->
                new TodoNotFoundException("Todo with id: " + todoId + " in the legal profile with id: " + profile.getProfileId() + " not found."));
    }

    //#endregion
}
