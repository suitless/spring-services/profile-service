package nl.suitless.profileservice.Config.Keycloak;

import org.apache.http.client.methods.HttpUriRequest;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableConfigurationProperties({KeycloakSpringBootProperties.class, KeycloakUserProperties.class})
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class KeycloakAdminRequestFactory extends KeycloakClientRequestFactory implements ClientHttpRequestFactory {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private final KeycloakSpringBootProperties props;
    private final KeycloakUserProperties userProps;

    @Autowired
    public KeycloakAdminRequestFactory(KeycloakSpringBootProperties props, KeycloakUserProperties userProps) {
        this.props = props;
        this.userProps = userProps;
    }

    public Keycloak getKeycloak(KeycloakSpringBootProperties props, KeycloakUserProperties userProps) {
        return KeycloakBuilder.builder()
                .serverUrl(props.getAuthServerUrl())
                .realm(props.getRealm())
                .clientId(props.getResource())
                .username(userProps.getUsername())
                .password(userProps.getPassword())
                .build();
    }

    @Override
    protected void postProcessHttpRequest(HttpUriRequest request) {
        String accessToken = getKeycloak(props, userProps).tokenManager().getAccessTokenString();
        request.setHeader(AUTHORIZATION_HEADER, "Bearer " + accessToken);
    }
}
