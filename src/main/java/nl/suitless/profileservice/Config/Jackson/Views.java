package nl.suitless.profileservice.Config.Jackson;

/**
 * Used for making different JSON Views to prevent infinite looping of entity relations
 */

public class Views {
    public static class Public {

    }

    public static class Summery extends Public {

    }

    public static class Internal extends Public {

    }
}
