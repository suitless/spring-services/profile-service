package nl.suitless.profileservice.Config.RoleManagement;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "authorization")
public class RoleManagementProperties {
    @NotBlank(message = "Role must be given in properties")
    private String[] roles;

    public RoleManagementProperties() {
    }

    @NotBlank
    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }
}
