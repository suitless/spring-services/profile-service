package nl.suitless.profileservice.Config.RoleManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(RoleManagementProperties.class)
public class RoleManagementConfig {
    private final RoleManagementProperties roleManagementProperties;

    @Autowired()
    public RoleManagementConfig(RoleManagementProperties roleManagementProperties) {
        this.roleManagementProperties = roleManagementProperties;
    }

    public String[] getRoles() {
        return this.roleManagementProperties.getRoles();
    }
}
