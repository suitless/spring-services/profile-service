package nl.suitless.profileservice.Config.Invitations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(InvitationProperties.class)
public class InvitationConfig {
    private final InvitationProperties invitationProperties;

    @Autowired
    public InvitationConfig(InvitationProperties invitationProperties) {
        this.invitationProperties = invitationProperties;
    }

    public String getBaseUrl() { return this.invitationProperties.getBaseUrl(); }
    public String getEmailHttpCall() { return this.invitationProperties.getEmailHttpCall(); }
    public int getDaysValid() { return this.invitationProperties.getDaysValid(); }
}
