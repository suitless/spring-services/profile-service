package nl.suitless.profileservice.Config.Invitations;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "invitations")
public class InvitationProperties {
    @NotBlank(message = "Basic invitation URL must be given in properties")
    @JsonProperty("base-url")
    private String baseUrl;

    @NotBlank(message = "Email http(s)-call must be given in properties")
    @JsonProperty("email-http-call")
    private String emailHttpCall;

    @NotBlank(message = "The amount of days valid must be given in properties")
    @JsonProperty("days-valid")
    private int daysValid;

    public InvitationProperties() {
    }

    @NotBlank
    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @NotBlank
    public String getEmailHttpCall() {
        return emailHttpCall;
    }

    public void setEmailHttpCall(String emailHttpCall) {
        this.emailHttpCall = emailHttpCall;
    }

    @NotBlank
    public int getDaysValid() {
        return daysValid;
    }

    public void setDaysValid(int daysValid) {
        this.daysValid = daysValid;
    }
}
