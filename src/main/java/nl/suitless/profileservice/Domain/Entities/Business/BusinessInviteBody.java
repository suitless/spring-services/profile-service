package nl.suitless.profileservice.Domain.Entities.Business;

public class BusinessInviteBody {
    private String email;
    private String businessLink;
    private String businessName;

    public BusinessInviteBody(String email, String businessLink, String businessName) {
        this.email = email;
        this.businessLink = businessLink;
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessLink() {
        return businessLink;
    }

    public void setBusinessLink(String businessLink) {
        this.businessLink = businessLink;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
}
