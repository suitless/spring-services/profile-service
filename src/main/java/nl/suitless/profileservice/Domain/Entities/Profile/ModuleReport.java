package nl.suitless.profileservice.Domain.Entities.Profile;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class ModuleReport {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String moduleReportId;
    @NotNull
    private String moduleId;
    @NotNull
    private String reportId;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "profileId", nullable = false)
    private Profile profile;

    public ModuleReport() {
    }

    public ModuleReport(@NotNull String moduleId, @NotNull String reportId) {
        this.moduleId = moduleId;
        this.reportId = reportId;
    }

    public void setModuleReportId(String moduleReportId) {
        this.moduleReportId = moduleReportId;
    }

    public String getModuleReportId() {
        return moduleReportId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
