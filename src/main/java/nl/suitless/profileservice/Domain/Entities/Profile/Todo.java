package nl.suitless.profileservice.Domain.Entities.Profile;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Todo {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @JsonView(Views.Public.class)
    private String todoId;
    @NotNull
    @JsonView(Views.Public.class)
    private String todoType;
    @NotNull
    @JsonView(Views.Public.class)
    private String assignment;
    @JsonView(Views.Public.class)
    private Boolean state;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "profileId", nullable = false)
    private Profile profile;

    public Todo() {
    }

    public Todo(@NotNull String assignment, @NotNull String todoType) {
        this.todoType = todoType;
        this.assignment = assignment;
        this.state = false;
    }

    public void setTodoId(String todoId) {
        this.todoId = todoId;
    }

    public String getTodoId() {
        return todoId;
    }

    public String getTodoType() {
        return todoType;
    }

    public void setTodoType(String todoType) {
        this.todoType = todoType;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
