package nl.suitless.profileservice.Domain.Entities.Profile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.ITagProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.TagNotFoundException;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Profile implements ITagProfile {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @JsonView(Views.Public.class)
    private String profileId;

    @JsonView(Views.Public.class)
    @NotNull
    private String accountId;

    @JsonView(Views.Internal.class)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "businessProfileId", referencedColumnName = "businessProfileId")
    private BusinessProfile businessProfile;

    @JsonView(Views.Public.class)
    @Enumerated(EnumType.STRING)
    @Column(length = 9)
    private Role role;

    @JsonView(Views.Public.class)
    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ModuleReport> moduleReports = new ArrayList<>();

    @JsonView(Views.Public.class)
    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Todo> todos = new ArrayList<>();

    @JsonView(Views.Public.class)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "profileStringTag",
            joinColumns = { @JoinColumn(name = "profileId") },
            inverseJoinColumns = @JoinColumn(name = "stringTagId")
    )
    private List<StringTag> stringTags = new ArrayList<>();

    @JsonView(Views.Public.class)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "profileDoubleTag",
            joinColumns = { @JoinColumn(name = "profileId") },
            inverseJoinColumns = @JoinColumn(name = "doubleTagId")
    )
    private List<DoubleTag> doubleTags = new ArrayList<>();

    public Profile() {
    }

    public Profile(@NotNull String accountId) {
        this.accountId = accountId;
    }

    public Profile(String profileId, @NotNull String accountId) {
        this.profileId = profileId;
        this.accountId = accountId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BusinessProfile getBusinessProfile() {
        return businessProfile;
    }

    public void setBusinessProfile(BusinessProfile businessProfile) {
        this.businessProfile = businessProfile;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<ModuleReport> getModuleReports() {
        return moduleReports;
    }

    public void setModuleReports(List<ModuleReport> moduleReports) {
        this.moduleReports = moduleReports;
    }

    public List<Todo> getTodos() {
        return todos;
    }

    public void setTodos(List<Todo> todos) {
        this.todos = todos;
    }

    //#region string tag methods

    public StringTag getStringTag(String key) {
        if(key != null && !key.isEmpty()) {
            return stringTags.stream().filter(tag -> tag.getKey().equals(key)).findFirst()
                    .orElseThrow(() -> new TagNotFoundException("tag with key: " + key + " not found."));
        } else {
            return null;
        }
    }

    @Override
    public List<StringTag> getStringTags() {
        return stringTags;
    }

    @Override
    public void setStringTags(List<StringTag> stringTags) {
        this.stringTags = stringTags;
    }

    //#endregion

    //#region double tag methods

    public DoubleTag getDoubleTag(String key) {
        if(key != null && !key.isEmpty()) {
            return doubleTags.stream().filter(tag -> tag.getKey().equals(key)).findFirst()
                    .orElseThrow(() -> new TagNotFoundException("tag with key: " + key + " not found."));
        } else {
            return null;
        }
    }

    @Override
    public List<DoubleTag> getDoubleTags() {
        return doubleTags;
    }

    @Override
    public void setDoubleTags(List<DoubleTag> doubleTags) {
        this.doubleTags = doubleTags;
    }

    //#endregion
}
