package nl.suitless.profileservice.Domain.Entities.Tags;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Exceptions.MapFalseException;
import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class DoubleTag implements ITag {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String doubleTagId;

    @JsonView(Views.Public.class)
    @NotNull
    @Column(name = "tag_name")
    private String key;

    @JsonView(Views.Public.class)
    @NotNull
    private double value;

    @ManyToMany(mappedBy = "doubleTags")
    private Set<BusinessProfile> businessProfiles = new HashSet<>();

    @ManyToMany(mappedBy = "doubleTags")
    private Set<Profile> profiles = new HashSet<>();

    public DoubleTag() {
    }

    public DoubleTag(@NotNull String key, @NotNull double value) {
        this.key = key;
        this.value = value;
    }

    public DoubleTag(DoubleTagRequestModel doubleTagWrapper) {
        this.key = doubleTagWrapper.getKey();
        this.value = doubleTagWrapper.getValue();
    }

    public static List<DoubleTag> mapToTagList(List<DoubleTagRequestModel> tags) {
        try {
            return tags == null || tags.isEmpty() ? new ArrayList<>() : tags.stream().map(DoubleTag::new).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new MapFalseException("cant map to DoubleTag list");
    }

    @JsonIgnore
    @Override
    public String getTagId() {
        return doubleTagId;
    }

    public void setDoubleTagId(String doubleTagId) {
        this.doubleTagId = doubleTagId;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        try{
            this.value = Double.parseDouble(value.toString());
        } catch (Exception e) {
            throw new MapFalseException("you haven't given a double value");
        }
    }

    @JsonIgnore
    public Set<BusinessProfile> getBusinessProfiles() {
        return businessProfiles;
    }

    public void setBusinessProfiles(Set<BusinessProfile> businessProfiles) {
        this.businessProfiles = businessProfiles;
    }

    @JsonIgnore
    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }
}
