package nl.suitless.profileservice.Domain.Entities.Tags;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Exceptions.MapFalseException;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class StringTag implements ITag{
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String stringTagId;

    @JsonView(Views.Public.class)
    @NotNull
    @Column(name = "tag_name")
    private String key;

    @JsonView(Views.Public.class)
    @NotNull
    private String value;

    @ManyToMany(mappedBy = "stringTags")
    private Set<BusinessProfile> businessProfiles = new HashSet<>();

    @ManyToMany(mappedBy = "stringTags")
    private Set<Profile> profiles = new HashSet<>();

    public StringTag() {
    }

    public StringTag(@NotNull String key, @NotNull String value) {
        this.key = key;
        this.value = value;
    }

    public StringTag(StringTagRequestModel stringTagWrapper) {
        this.key = stringTagWrapper.getKey();
        this.value = stringTagWrapper.getValue();
    }

    public static List<StringTag> mapToTagList(List<StringTagRequestModel> tags) {
        try{
            return tags == null || tags.isEmpty() ? new ArrayList<>() : tags.stream().map(StringTag::new).collect(Collectors.toList());
        } catch (Exception e){
            e.printStackTrace();
        }

        throw new MapFalseException("cant map to StringTag list");
    }

    @JsonIgnore
    @Override
    public String getTagId() {
        return stringTagId;
    }

    public void setStringTagId(String stringTagId) {
        this.stringTagId = stringTagId;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        try{
            this.value = (String) value;
        } catch (Exception e) {
            throw new MapFalseException("You haven't given a string value");
        }
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonIgnore
    public Set<BusinessProfile> getBusinessProfiles() {
        return businessProfiles;
    }

    public void setBusinessProfiles(Set<BusinessProfile> businessProfiles) {
        this.businessProfiles = businessProfiles;
    }

    @JsonIgnore
    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }
}
