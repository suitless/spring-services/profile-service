package nl.suitless.profileservice.Domain.Entities.Tags;

public interface ITag {
    String key = "";
    Object value = null;

    String getTagId();
    String getKey();
    void setValue(Object tag);
}
