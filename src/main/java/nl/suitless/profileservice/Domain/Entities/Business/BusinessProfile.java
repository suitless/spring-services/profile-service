package nl.suitless.profileservice.Domain.Entities.Business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Domain.Entities.Tags.ITagProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.TagNotFoundException;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BusinessProfile implements ITagProfile {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @JsonView(Views.Public.class)
    private String businessProfileId;

    @JsonView(Views.Public.class)
    @NotNull
    private String businessName;

    @JsonView(Views.Public.class)
    private String templateAssetId;

    @JsonView(Views.Public.class)
    @NotNull
    @ElementCollection
    private List<String> ownerIds;

    @JsonView(Views.Public.class)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "businessProfileStringTag",
            joinColumns = {@JoinColumn(name = "businessProfileId")},
            inverseJoinColumns = @JoinColumn(name = "stringTagId")
    )
    private List<StringTag> stringTags = new ArrayList<>();

    @JsonView(Views.Public.class)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "businessProfileDoubleTag",
            joinColumns = {@JoinColumn(name = "businessProfileId")},
            inverseJoinColumns = @JoinColumn(name = "doubleTagId")
    )
    private List<DoubleTag> doubleTags = new ArrayList<>();

    @JsonView(Views.Summery.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "businessProfile")
    private List<Profile> profiles = new ArrayList<>();

    public BusinessProfile() {
    }

    public BusinessProfile(@NotNull String businessName, @NotNull List<String> ownerIds) {
        this.businessName = businessName;
        this.ownerIds = ownerIds;
    }

    public BusinessProfile(@NotNull String businessName, String templateAssetId, @NotNull List<String> ownerIds,
                           List<StringTag> stringTags, List<DoubleTag> doubleTags, List<Profile> profiles) {
        this.businessName = businessName;
        this.templateAssetId = templateAssetId;
        this.ownerIds = ownerIds;
        this.stringTags = stringTags;
        this.doubleTags = doubleTags;
        this.profiles = profiles;
    }

    public String getBusinessProfileId() {
        return businessProfileId;
    }

    public void setBusinessProfileId(String businessProfileId) {
        this.businessProfileId = businessProfileId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTemplateAssetId() {
        return templateAssetId;
    }

    public void setTemplateAssetId(String templateAssetId) {
        this.templateAssetId = templateAssetId;
    }

    public List<String> getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(List<String> ownerIds) {
        this.ownerIds = ownerIds;
    }

    @JsonIgnore
    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    //#region string tag methods

    public StringTag getStringTag(String key) {
        if(key != null && !key.isEmpty()) {
            return stringTags.stream().filter(tag -> tag.getKey().equals(key)).findFirst()
                    .orElseThrow(() -> new TagNotFoundException("tag with key: " + key + " not found."));
        } else {
            return null;
        }
    }

    @Override
    public List<StringTag> getStringTags() {
        return stringTags;
    }

    @Override
    public void setStringTags(List<StringTag> stringTags) {
        this.stringTags = stringTags;
    }

    //#endregion

    //#region double tag methods

    public DoubleTag getDoubleTag(String key) {
        if(key != null && !key.isEmpty()) {
            return doubleTags.stream().filter(tag -> tag.getKey().equals(key)).findFirst()
                    .orElseThrow(() -> new TagNotFoundException("tag with key: " + key + " not found."));
        } else {
            return null;
        }
    }

    @Override
    public List<DoubleTag> getDoubleTags() {
        return doubleTags;
    }

    @Override
    public void setDoubleTags(List<DoubleTag> doubleTags) {
        this.doubleTags = doubleTags;
    }

    //#endregion
}
