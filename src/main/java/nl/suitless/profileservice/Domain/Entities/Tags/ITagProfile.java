package nl.suitless.profileservice.Domain.Entities.Tags;

import java.util.ArrayList;
import java.util.List;

public interface ITagProfile {
    List<StringTag> stringTags = new ArrayList<>();
    List<DoubleTag> doubleTags = new ArrayList<>();

    void setStringTags(List<StringTag> stringTags);
    List<StringTag> getStringTags();

    void setDoubleTags(List<DoubleTag> doubleTags);
    List<DoubleTag> getDoubleTags();
}
