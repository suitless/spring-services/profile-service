package nl.suitless.profileservice.Domain.Entities.Profile;

/**
 * Theses roles are used as business roles
 * For instance an account contains a profile that contains a list of roles the person has within a company:
 * Like accountant, manager etc...
 * The roles within the account profile are the access specific roles like: Admin, User, Editor
 */

public enum Role {
    LAW,
    TAX,
    FINANCIAL,
    ALL
}
