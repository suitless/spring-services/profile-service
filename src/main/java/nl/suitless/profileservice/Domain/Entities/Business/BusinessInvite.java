package nl.suitless.profileservice.Domain.Entities.Business;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class BusinessInvite {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String businessInviteId;
    @NotNull
    private String email;
    @NotNull
    private Date expirationDate;
    @NotNull
    private String businessId;

    public BusinessInvite() {
    }

    public BusinessInvite(@NotNull String email, @NotNull Date expirationDate, @NotNull String businessId) {
        this.email = email;
        this.expirationDate = expirationDate;
        this.businessId = businessId;
    }

    public String getBusinessInviteId() {
        return businessInviteId;
    }

    public void setBusinessInviteId(String businessInviteId) {
        this.businessInviteId = businessInviteId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
