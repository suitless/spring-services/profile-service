package nl.suitless.profileservice.Domain.Exceptions;

public class MapFalseException extends RuntimeException {
    public MapFalseException(String exception) { super(exception); }
}
