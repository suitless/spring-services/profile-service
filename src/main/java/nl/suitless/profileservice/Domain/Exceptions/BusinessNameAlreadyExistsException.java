package nl.suitless.profileservice.Domain.Exceptions;

public class BusinessNameAlreadyExistsException extends RuntimeException {
    public BusinessNameAlreadyExistsException(String exception) { super(exception); }
}
