package nl.suitless.profileservice.Domain.Exceptions;

public class BusinessProfileNotFoundException extends RuntimeException {
    public BusinessProfileNotFoundException(String exception) { super(exception); }
}
