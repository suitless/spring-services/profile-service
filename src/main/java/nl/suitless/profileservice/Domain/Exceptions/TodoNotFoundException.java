package nl.suitless.profileservice.Domain.Exceptions;

public class TodoNotFoundException extends RuntimeException {
    public TodoNotFoundException(String exception){super(exception);}
}
