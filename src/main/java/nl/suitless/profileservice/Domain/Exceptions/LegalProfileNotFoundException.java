package nl.suitless.profileservice.Domain.Exceptions;

public class LegalProfileNotFoundException extends RuntimeException {
    public LegalProfileNotFoundException(String exception) { super(exception); }
}
