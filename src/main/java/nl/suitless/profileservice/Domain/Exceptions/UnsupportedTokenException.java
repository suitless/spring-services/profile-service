package nl.suitless.profileservice.Domain.Exceptions;

public class UnsupportedTokenException extends RuntimeException {
    public UnsupportedTokenException(String exception) { super(exception); }
}
