package nl.suitless.profileservice.Domain.Exceptions;

public class TagAlreadyExistsException extends RuntimeException {
    public TagAlreadyExistsException(String exception) { super(exception); }
}
