package nl.suitless.profileservice.Domain.Exceptions;

public class CantRemoveAccountFromBusinessProfileException extends RuntimeException {
    public CantRemoveAccountFromBusinessProfileException(String exception) { super(exception); }
}
