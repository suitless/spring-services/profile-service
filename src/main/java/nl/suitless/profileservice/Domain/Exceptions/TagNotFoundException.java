package nl.suitless.profileservice.Domain.Exceptions;

public class TagNotFoundException extends RuntimeException {
    public TagNotFoundException(String exception) { super(exception); }
}
