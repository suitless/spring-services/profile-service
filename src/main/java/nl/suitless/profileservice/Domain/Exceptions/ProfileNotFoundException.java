package nl.suitless.profileservice.Domain.Exceptions;

public class ProfileNotFoundException extends RuntimeException {
    public ProfileNotFoundException(String exception) { super(exception); }
}
