package nl.suitless.profileservice.Domain.Exceptions;

public class InvitationException extends RuntimeException {
    public InvitationException(String exception) { super(exception); }
}
