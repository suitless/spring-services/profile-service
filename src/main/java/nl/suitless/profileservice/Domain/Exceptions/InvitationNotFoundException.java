package nl.suitless.profileservice.Domain.Exceptions;

public class InvitationNotFoundException extends RuntimeException {
    public InvitationNotFoundException(String exception) { super(exception); }
}
