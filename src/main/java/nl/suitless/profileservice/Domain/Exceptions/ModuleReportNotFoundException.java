package nl.suitless.profileservice.Domain.Exceptions;

public class ModuleReportNotFoundException extends RuntimeException {
    public ModuleReportNotFoundException(String exception) {super(exception);}
}
