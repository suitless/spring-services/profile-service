package nl.suitless.profileservice.Domain.Exceptions;

public class UnauthorizedAccount extends RuntimeException {
    public UnauthorizedAccount(String exception) { super(exception);}
}
