package nl.suitless.profileservice.Domain.Exceptions;

public class AccountAlreadyInABusinessException extends RuntimeException {
    public AccountAlreadyInABusinessException(String exception) { super(exception);}
}
