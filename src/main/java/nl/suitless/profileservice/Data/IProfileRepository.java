package nl.suitless.profileservice.Data;

import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IProfileRepository extends CrudRepository<Profile, String> {
    Optional<Profile> findTopByAccountId(String accountId);
}
