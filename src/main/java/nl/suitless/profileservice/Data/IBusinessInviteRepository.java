package nl.suitless.profileservice.Data;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessInvite;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IBusinessInviteRepository extends CrudRepository<BusinessInvite, String> {
    Optional<BusinessInvite> getByEmailAndBusinessId(String email, String businessId);
}
