package nl.suitless.profileservice.Data;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IBusinessProfileRepository extends CrudRepository<BusinessProfile, String> {
    Optional<BusinessProfile> getByBusinessName(String businessName);
}
