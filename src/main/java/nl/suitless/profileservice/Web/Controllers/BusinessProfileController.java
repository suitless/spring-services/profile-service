package nl.suitless.profileservice.Web.Controllers;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Domain.Exceptions.TagNotFoundException;
import nl.suitless.profileservice.Domain.Exceptions.UnsupportedTokenException;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessProfileService;
import nl.suitless.profileservice.Web.HateoasResources.BusinessProfileResource;
import nl.suitless.profileservice.Web.HateoasResources.BusinessProfilesResource;
import nl.suitless.profileservice.Web.HateoasResources.UsersResource;
import nl.suitless.profileservice.Web.Utils.KeycloakUtils;
import nl.suitless.profileservice.Web.Wrapper.businessProfile.AddAccountRequestModel;
import nl.suitless.profileservice.Web.Wrapper.businessProfile.UpdateBusinessProfileRequestModel;
import nl.suitless.profileservice.Web.Wrapper.businessProfile.CreateBusinessProfileRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequestMapping(path = "/business")
@RestController
public class BusinessProfileController {
    private final IBusinessProfileService businessProfileService;

    @Autowired
    public BusinessProfileController(IBusinessProfileService profileService) {
        this.businessProfileService = profileService;
    }

    @JsonView(Views.Summery.class)
    @PostMapping(path = "/")
    public ResponseEntity<BusinessProfileResource> createBusinessProfile(Principal principal, @RequestBody CreateBusinessProfileRequestModel reqModel) {
        List<StringTag> stringTags = StringTag.mapToTagList(reqModel.getStringTags());
        List<DoubleTag> doubleTags = DoubleTag.mapToTagList(reqModel.getDoubleTags());

        BusinessProfile businessProfile = businessProfileService.createBusinessProfile(KeycloakUtils.getAccountId(principal),
                reqModel.getBusinessName(), reqModel.getTemplateAssetId(), reqModel.getOwnerIds(), stringTags, doubleTags);

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).createBusinessProfile(principal, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(BusinessProfileController.class).updateBusinessProfile(
                principal, businessProfile.getBusinessProfileId(), new UpdateBusinessProfileRequestModel())).withRel("update business profile"));
        resource.add(linkTo(methodOn(BusinessProfileController.class).deleteBusinessProfile(principal,
                resource.getBusinessProfile().getBusinessProfileId())).withRel("delete business profile"));

        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Summery.class)
    @GetMapping(path = "/all")
    public ResponseEntity<BusinessProfilesResource> getAllBusinessProfiles(Principal principal) {
        BusinessProfilesResource resource = new BusinessProfilesResource(
                businessProfileService.getAllBusinessProfiles(getRealmAccess(principal)));

        resource.add(linkTo(methodOn(BusinessProfileController.class).getAllBusinessProfiles(principal)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @PutMapping(path = "/{businessProfileId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BusinessProfileResource> updateBusinessProfile(Principal principal,
                                                                         @PathVariable("businessProfileId") String id,
                                                                         @RequestBody UpdateBusinessProfileRequestModel reqModel) {
        List<StringTag> stringTags = StringTag.mapToTagList(reqModel.getStringTags());
        List<DoubleTag> doubleTags = DoubleTag.mapToTagList(reqModel.getDoubleTags());
        DoubleTag amountOfEmployees = doubleTags.stream().filter(t -> t.getKey().equals("amountOfEmployees")).findFirst()
                .orElseThrow(() -> new TagNotFoundException("Could not find the amountOfEmployees tag did you delete it?"));
        doubleTags.remove(amountOfEmployees);

        BusinessProfile businessProfile = businessProfileService.updateBusinessProfile(KeycloakUtils.getAccountId(principal),
                id, reqModel.getBusinessName(), reqModel.getTemplateAssetId(), reqModel.getOwnerIds(), stringTags, doubleTags);

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).updateBusinessProfile(principal, id, reqModel)).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @GetMapping(path = "/{businessProfileId}")
    public ResponseEntity<BusinessProfileResource> getBusinessProfileById(Principal principal, @PathVariable("businessProfileId") String id) {
        BusinessProfileResource resource = new BusinessProfileResource(businessProfileService.getBusinessProfileById(id));

        resource.add(linkTo(methodOn(BusinessProfileController.class).getBusinessProfileById(principal, id)).withSelfRel());
        resource.add(linkTo(methodOn(BusinessProfileController.class).deleteBusinessProfile(principal,
                resource.getBusinessProfile().getBusinessProfileId())).withRel("delete business profile"));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = "/{businessProfileId}/users")
    public ResponseEntity<UsersResource> getUsersFromBusinessProfile(Principal principal, @PathVariable("businessProfileId") String id) {
        UsersResource resource = new UsersResource(
                businessProfileService.getUsersFromBusinessProfile(KeycloakUtils.getAccountId(principal), id));

        resource.add(linkTo(methodOn(BusinessProfileController.class).getUsersFromBusinessProfile(principal, id)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{businessProfileId}")
    public ResponseEntity<Void> deleteBusinessProfile(Principal principal, @PathVariable("businessProfileId") String id) {
        businessProfileService.deleteBusinessProfile(getRealmAccess(principal), id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @PostMapping(path = "{businessProfileId}/tag/string")
    public ResponseEntity<BusinessProfileResource> addStringTag(@PathVariable("businessProfileId") String id, @RequestBody StringTagRequestModel reqModel) {
        BusinessProfile businessProfile = businessProfileService.addTag(id, reqModel.getKey(), reqModel.getValue());

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).addStringTag(id, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(BusinessProfileController.class).editStringTag(id, reqModel)).withRel("edit string tag"));
        businessProfile.getStringTags().stream().filter(tag -> tag.getKey().equals(reqModel.getKey())).findFirst().ifPresent(tag ->
                resource.add(linkTo(methodOn(BusinessProfileController.class).deleteTag(id, tag.getTagId())).withRel("delete string tag")));

        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Summery.class)
    @PutMapping(path = "/{businessProfileId}/tag/string")
    public ResponseEntity<BusinessProfileResource> editStringTag(@PathVariable("businessProfileId") String id, @RequestBody StringTagRequestModel reqModel) {
        BusinessProfile businessProfile = businessProfileService.editTag(id, reqModel.getKey(), reqModel.getValue());

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).editStringTag(id, reqModel)).withSelfRel());
        businessProfile.getStringTags().stream().filter(tag -> tag.getKey().equals(reqModel.getKey())).findFirst().ifPresent(tag ->
                resource.add(linkTo(methodOn(BusinessProfileController.class).deleteTag(id, tag.getTagId())).withRel("delete string tag")));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @PostMapping(path = "/{businessProfileId}/tag/double")
    public ResponseEntity<BusinessProfileResource> addDoubleTag(@PathVariable("businessProfileId") String id, @RequestBody DoubleTagRequestModel reqModel) {
        BusinessProfile businessProfile = businessProfileService.addTag(id, reqModel.getKey(), reqModel.getValue());

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).addDoubleTag(id, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(BusinessProfileController.class).editDoubleTag(id, reqModel)).withRel("edit double tag"));
        businessProfile.getDoubleTags().stream().filter(tag -> tag.getKey().equals(reqModel.getKey())).findFirst().ifPresent(tag ->
                resource.add(linkTo(methodOn(BusinessProfileController.class).deleteTag(id, tag.getTagId())).withRel("delete double tag")));

        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Summery.class)
    @PutMapping(path = "/{businessProfileId}/tag/double")
    public ResponseEntity<BusinessProfileResource> editDoubleTag(@PathVariable("businessProfileId") String id, @RequestBody DoubleTagRequestModel reqModel) {
        BusinessProfile businessProfile = businessProfileService.editTag(id, reqModel.getKey(), reqModel.getValue());

        BusinessProfileResource resource = new BusinessProfileResource(businessProfile);
        resource.add(linkTo(methodOn(BusinessProfileController.class).editDoubleTag(id, reqModel)).withSelfRel());
        businessProfile.getDoubleTags().stream().filter(tag -> tag.getKey().equals(reqModel.getKey())).findFirst().ifPresent(tag ->
                resource.add(linkTo(methodOn(BusinessProfileController.class).deleteTag(id, tag.getTagId())).withRel("delete double tag")));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @DeleteMapping(path = "/{businessProfileId}/tag/{key}")
    public ResponseEntity<BusinessProfileResource> deleteTag(@PathVariable("businessProfileId") String id, @PathVariable("key") String key) {
        BusinessProfileResource resource = new BusinessProfileResource(businessProfileService.deleteTag(id, key));
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @DeleteMapping(path = {"/{businessProfileId}/account/{accountId}"})
    public ResponseEntity<BusinessProfileResource> deleteAccountFromBusinessProfile(Principal principal,
                                                                                    @PathVariable("businessProfileId") String id,
                                                                                    @PathVariable("accountId") String accountId) {
        BusinessProfileResource resource =
                new BusinessProfileResource(businessProfileService.deleteAccount(KeycloakUtils.getAccountId(principal), id, accountId));

        resource.add(linkTo(methodOn(BusinessProfileController.class).deleteAccountFromBusinessProfile(principal, id, accountId)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Summery.class)
    @GetMapping(path = "/check/{businessId}")
    public ResponseEntity<Boolean> checkAccountInBusiness(@PathVariable("businessId") String businessId, Principal principal) {
        boolean checkResult = businessProfileService.checkAccountInBusiness(businessId, KeycloakUtils.getAccountId(principal));

        return new ResponseEntity<>(checkResult, HttpStatus.OK);
    }

    private Collection<GrantedAuthority> getRealmAccess(Principal principal) {
        //needed for public api calls
        if (principal == null) {
            return null;
        }

        if(principal instanceof KeycloakAuthenticationToken) {
            return ((KeycloakAuthenticationToken) principal).getAuthorities();
        } else {
            throw new UnsupportedTokenException("give token is not supported");
        }
    }
}