package nl.suitless.profileservice.Web.Controllers;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Tags.DoubleTag;
import nl.suitless.profileservice.Domain.Entities.Tags.StringTag;
import nl.suitless.profileservice.Services.Interfaces.IProfileService;
import nl.suitless.profileservice.Web.HateoasResources.ProfileResource;
import nl.suitless.profileservice.Web.Utils.KeycloakUtils;
import nl.suitless.profileservice.Web.Wrapper.profile.*;
import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("")
public class ProfileController {
    private final IProfileService profileService;

    @Autowired
    public ProfileController(IProfileService profileService) {
        this.profileService = profileService;
    }

    @JsonView(Views.Internal.class)
    @GetMapping("/my")
    public ResponseEntity<ProfileResource> getProfileByAccountId(Principal principal) {
        ProfileResource resource = new ProfileResource(profileService.getProfileByAccountId(KeycloakUtils.getAccountId(principal)));
        resource.add(linkTo(methodOn(ProfileController.class).getProfileByAccountId(principal)).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(value = {"/profile/business", "/profile/business/account/{accountId}"})
    public ResponseEntity<ProfileResource> createProfileAndBusiness(Principal principal,
                                                                    @PathVariable(required = false) String accountId,
                                                                    @RequestBody CreateProfileAndBusinessRequestModel reqModel) {
        List<StringTag> businessStringTags = StringTag.mapToTagList(reqModel.getBusinessStringTags());
        List<DoubleTag> businessDoubleTags = DoubleTag.mapToTagList(reqModel.getBusinessDoubleTags());
        List<StringTag> stringTags = StringTag.mapToTagList(reqModel.getStringTags());
        List<DoubleTag> doubleTags = DoubleTag.mapToTagList(reqModel.getDoubleTags());
        String id = accountId != null ? accountId : KeycloakUtils.getAccountId(principal);

        ProfileResource resource = new ProfileResource(profileService.createProfile(id,
                reqModel.getRole(), reqModel.getBusinessName(), reqModel.getTemplateAssetId(), reqModel.getOwnerIds(),
                reqModel.getAmountOfEmployees(), businessStringTags, businessDoubleTags, stringTags, doubleTags));

        resource.add(linkTo(methodOn(ProfileController.class)
                .createProfileAndBusiness(principal, accountId, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(value = {"/", "/profile/business/{businessId}"})
    public ResponseEntity<ProfileResource> createProfile(Principal principal,
                                                         @PathVariable(required = false) String businessId,
                                                         @RequestBody(required = false) CreateProfileRequestModel reqModel) {
        List<StringTag> stringTags = StringTag.mapToTagList(reqModel.getStringTags());
        List<DoubleTag> doubleTags = DoubleTag.mapToTagList(reqModel.getDoubleTags());

        ProfileResource resource = businessId != null ?
                new ProfileResource(profileService.createProfile(KeycloakUtils.getAccountId(principal), reqModel.getRole(),
                    businessId, stringTags, doubleTags)) :
                new ProfileResource(profileService.createProfile(KeycloakUtils.getAccountId(principal), reqModel.getRole(),
                    stringTags, doubleTags));

        resource.add(linkTo(methodOn(ProfileController.class).createProfile(principal, businessId, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Internal.class)
    @PutMapping("/{profileId}")
    public ResponseEntity<ProfileResource> updateProfile(@PathVariable String profileId, @RequestBody EditProfileRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.updateProfile(profileId, reqModel.getRole()));

        resource.add(linkTo(methodOn(ProfileController.class).updateProfile(profileId, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @DeleteMapping("/{profileId}")
    public ResponseEntity<ProfileResource> deleteProfile(@PathVariable("profileId") String id) {
        profileService.deleteProfile(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(path = "/{profileId}/module")
    public ResponseEntity<ProfileResource> addModuleReport(@PathVariable("profileId") String id, @RequestBody CreateModelRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.addModuleReports(id, reqModel.getModuleId(), reqModel.getReportId()));

        resource.add(linkTo(methodOn(ProfileController.class).addModuleReport(id, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Internal.class)
    @DeleteMapping(path = "/{profileId}/module/{moduleId}")
    public ResponseEntity<Void> deleteModuleReport(@PathVariable("profileId") String id, @PathVariable("moduleId") String moduleId) {
        profileService.deleteModuleReport(id, moduleId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(path = "/{profileId}/todo")
    public ResponseEntity<ProfileResource> addTodo(@PathVariable("profileId") String id,
                                                   @RequestBody CreateTodoRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.addTodo(id, reqModel.getTodoType(), reqModel.getAssignment()));

        resource.add(linkTo(methodOn(ProfileController.class).addTodo(id, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Internal.class)
    @PutMapping(path = "/{profileId}/todo/{todoId}")
    public ResponseEntity<ProfileResource> editTodo(@PathVariable("profileId") String id,
                                                    @PathVariable("todoId") String todoId,
                                                    @RequestBody EditTodoRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(
                profileService.editTodo(id, todoId, reqModel.getTodoType(), reqModel.getAssignment(), reqModel.isState()));

        resource.add(linkTo(methodOn(ProfileController.class).editTodo(id, todoId, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(ProfileController.class).deleteTodo(id, todoId)).withRel("Delete to do"));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @DeleteMapping(path = "/{profileId}/todo/{todoId}")
    public ResponseEntity<ProfileResource> deleteTodo(@PathVariable("profileId") String id, @PathVariable("todoId") String todoId) {
        profileService.deleteTodo(id, todoId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(path = "/{profileId}/tag/string")
    public ResponseEntity<ProfileResource> addStringTag(@PathVariable("profileId") String id, @RequestBody StringTagRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.addTag(id, reqModel.getKey(), reqModel.getValue()));

        resource.add(linkTo(methodOn(ProfileController.class).addStringTag(id, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(ProfileController.class).editStringTag(id, reqModel)).withRel("edit string tag"));
        resource.add(linkTo(methodOn(ProfileController.class).deleteTag(id, reqModel.getKey())).withRel("delete string tag"));
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @JsonView(Views.Internal.class)
    @PutMapping(path = "/{profileId}/tag/string")
    public ResponseEntity<ProfileResource> editStringTag(@PathVariable("profileId") String id, @RequestBody StringTagRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.editTag(id, reqModel.getKey(), reqModel.getValue()));

        resource.add(linkTo(methodOn(ProfileController.class).editStringTag(id, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PostMapping(path = "/{profileId}/tag/double")
    public ResponseEntity<ProfileResource> addDoubleTag(@PathVariable("profileId") String id, @RequestBody DoubleTagRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.addTag(id, reqModel.getKey(), reqModel.getValue()));

        resource.add(linkTo(methodOn(ProfileController.class).addDoubleTag(id, reqModel)).withSelfRel());
        resource.add(linkTo(methodOn(ProfileController.class).editDoubleTag(id, reqModel)).withRel("edit double tag"));
        resource.add(linkTo(methodOn(ProfileController.class).deleteTag(id, reqModel.getKey())).withRel("delete double tag"));
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @PutMapping(path = "/{profileId}/tag/double")
    public ResponseEntity<ProfileResource> editDoubleTag(@PathVariable("profileId") String id, @RequestBody DoubleTagRequestModel reqModel) {
        ProfileResource resource = new ProfileResource(profileService.editTag(id, reqModel.getKey(), reqModel.getValue()));

        resource.add(linkTo(methodOn(ProfileController.class).editDoubleTag(id, reqModel)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @JsonView(Views.Internal.class)
    @DeleteMapping(path = "/{profileId}/tag/{key}")
    public ResponseEntity<ProfileResource> deleteTag(@PathVariable("profileId") String id, @PathVariable("key") String key) {
        ProfileResource resource = new ProfileResource(profileService.deleteTag(id, key));

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }
}
