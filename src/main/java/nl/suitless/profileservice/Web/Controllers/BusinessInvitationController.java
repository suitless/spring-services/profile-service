package nl.suitless.profileservice.Web.Controllers;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Services.Business.Interfaces.IBusinessInviteService;
import nl.suitless.profileservice.Web.HateoasResources.BusinessProfileResource;
import nl.suitless.profileservice.Web.Utils.KeycloakUtils;
import nl.suitless.profileservice.Web.Wrapper.businessInvite.CreateInviteRequestModel;
import nl.suitless.profileservice.Web.Wrapper.businessInvite.InviteRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequestMapping(path = "/business/invite")
@RestController
public class BusinessInvitationController {
    private final IBusinessInviteService businessInviteService;

    @Autowired
    public BusinessInvitationController(IBusinessInviteService businessInviteService) {
        this.businessInviteService = businessInviteService;
    }

    @JsonView(Views.Summery.class)
    @GetMapping(path = "/{businessInviteId}")
    public ResponseEntity<BusinessProfileResource> getBusinessProfileByInviteId(@PathVariable("businessInviteId") String id) {
        BusinessProfileResource resource = new BusinessProfileResource(businessInviteService.getBusinessProfileByInviteId(id));
        resource.add(linkTo(methodOn(BusinessInvitationController.class).getBusinessProfileByInviteId(id)).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @PostMapping(path = "")
    public ResponseEntity<Void> createInvite(Principal principal, @RequestBody CreateInviteRequestModel reqModel) {
        businessInviteService.sendInvite(KeycloakUtils.getAccountId(principal), reqModel.getEmail(), reqModel.getBusinessProfile());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/accept/{id}")
    public ResponseEntity<Void> acceptInvite(Principal principal, @PathVariable("id") String id) {
        businessInviteService.acceptInvite(KeycloakUtils.getAccountId(principal), id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/decline/{id}")
    public ResponseEntity<Void> declineInvite(@PathVariable("id") String id) {
        businessInviteService.declineInvite(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
