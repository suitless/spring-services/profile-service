package nl.suitless.profileservice.Web.Utils;

import nl.suitless.profileservice.Domain.Exceptions.UnsupportedTokenException;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;

import java.security.Principal;

public class KeycloakUtils {
    public static String getAccountId(Principal principal) {
        if(principal instanceof KeycloakAuthenticationToken) {
            return ((KeycloakAuthenticationToken) principal).getAccount().getKeycloakSecurityContext().getToken().getSubject();
        } else {
            throw new UnsupportedTokenException("give token is not supported");
        }
    }
}
