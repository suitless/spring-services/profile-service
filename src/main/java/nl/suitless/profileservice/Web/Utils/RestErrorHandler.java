package nl.suitless.profileservice.Web.Utils;

import nl.suitless.profileservice.Domain.Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class RestErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccountAlreadyInABusinessException.class)
    public final ResponseEntity<ErrorDetails> handleAccountAlreadyInACompanyException (AccountAlreadyInABusinessException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = BusinessNameAlreadyExistsException.class)
    public final ResponseEntity<ErrorDetails> handleBusinessNameAlreadyExistsException (BusinessNameAlreadyExistsException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = BusinessProfileNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleBusinessProfileNotFoundException (BusinessProfileNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = CantRemoveAccountFromBusinessProfileException.class)
    public final ResponseEntity<ErrorDetails> handleCantRemoveAccountFromCompanyProfileException (CantRemoveAccountFromBusinessProfileException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = InvitationException.class)
    public final ResponseEntity<ErrorDetails> handleInvitationException(InvitationException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = InvitationNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleInvitationNotFoundException(InvitationNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = LegalProfileNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleLegalProfileNotFoundException (LegalProfileNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MapFalseException.class)
    public final ResponseEntity<ErrorDetails> handleMapFalseException (MapFalseException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ModuleReportNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleModuleReportNotFoundException (ModuleReportNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ProfileNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleProfileNotFoundException(ProfileNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TagAlreadyExistsException.class)
    public final ResponseEntity<ErrorDetails> handleTagAlreadyExistsException(TagAlreadyExistsException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = TagNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleFlagNotFoundException (TagNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TodoNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleTodoNotFoundException (TodoNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = UnauthorizedAccount.class)
    public final ResponseEntity<ErrorDetails> handleUnauthorizedException (UnauthorizedAccount e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = UnsupportedTokenException.class)
    public final ResponseEntity<ErrorDetails> handleUnsupportedTokenException(UnsupportedTokenException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
