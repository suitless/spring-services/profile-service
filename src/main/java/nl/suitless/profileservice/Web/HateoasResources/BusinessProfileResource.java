package nl.suitless.profileservice.Web.HateoasResources;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Web.Controllers.BusinessProfileController;
import org.springframework.hateoas.RepresentationModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class BusinessProfileResource extends RepresentationModel<BusinessProfileResource> {
    @JsonView(Views.Summery.class)
    private BusinessProfile businessProfile;

    public BusinessProfileResource(BusinessProfile businessProfile) {
        this.businessProfile = businessProfile;
        add(linkTo(BusinessProfileController.class).withRel("business profile"));
    }

    public BusinessProfileResource() {
    }

    public BusinessProfile getBusinessProfile() {
        return businessProfile;
    }
}
