package nl.suitless.profileservice.Web.HateoasResources;

import nl.suitless.profileservice.Domain.Entities.User;
import nl.suitless.profileservice.Web.Controllers.BusinessProfileController;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class UsersResource extends RepresentationModel<UsersResource> {
    private List<User> users;

    public UsersResource(List<User> users) {
        this.users = users;
        add(linkTo(BusinessProfileController.class).withSelfRel());
    }

    public UsersResource() {
    }

    public List<User> getUsers() {
        return users;
    }
}
