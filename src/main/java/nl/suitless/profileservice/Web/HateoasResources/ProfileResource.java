package nl.suitless.profileservice.Web.HateoasResources;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Profile.Profile;
import nl.suitless.profileservice.Web.Controllers.ProfileController;
import org.springframework.hateoas.RepresentationModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class ProfileResource extends RepresentationModel<ProfileResource> {
    @JsonView(Views.Public.class)
    public Profile profile;

    public ProfileResource(Profile profile) {
        this.profile = profile;
        add(linkTo(ProfileController.class).withRel("profile"));
    }

    public ProfileResource() {
    }

    public Profile getProfile() {
        return profile;
    }
}
