package nl.suitless.profileservice.Web.HateoasResources;

import com.fasterxml.jackson.annotation.JsonView;
import nl.suitless.profileservice.Config.Jackson.Views;
import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;
import nl.suitless.profileservice.Web.Controllers.BusinessProfileController;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class BusinessProfilesResource extends RepresentationModel<BusinessProfilesResource> {
    @JsonView(Views.Public.class)
    private List<BusinessProfile> businessProfiles;

    public BusinessProfilesResource(List<BusinessProfile> businessProfiles) {
        this.businessProfiles = businessProfiles;
        add(linkTo(BusinessProfileController.class).withRel("business profile"));
    }

    public BusinessProfilesResource() {
    }

    public List<BusinessProfile> getBusinessProfiles() {
        return businessProfiles;
    }
}
