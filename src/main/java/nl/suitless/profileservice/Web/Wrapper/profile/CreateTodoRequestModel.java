package nl.suitless.profileservice.Web.Wrapper.profile;

import javax.validation.constraints.NotNull;

public class CreateTodoRequestModel {
    @NotNull
    private String todoType;
    @NotNull
    private String assignment;

    public CreateTodoRequestModel() {
    }

    public String getTodoType() {
        return todoType;
    }

    public void setTodoType(String todoType) {
        this.todoType = todoType;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }
}
