package nl.suitless.profileservice.Web.Wrapper.profile;

import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateProfileAndBusinessRequestModel {
    @NotNull
    private Role role;
    @NotNull
    private String businessName;
    private String templateAssetId;
    private List<String> ownerIds;
    @NotNull
    private int amountOfEmployees;
    private List<StringTagRequestModel> businessStringTags;
    private List<DoubleTagRequestModel> businessDoubleTags;
    private List<StringTagRequestModel> stringTags;
    private List<DoubleTagRequestModel> doubleTags;

    public CreateProfileAndBusinessRequestModel() {
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTemplateAssetId() {
        return templateAssetId;
    }

    public void setTemplateAssetId(String templateAssetId) {
        this.templateAssetId = templateAssetId;
    }

    public List<String> getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(List<String> ownerIds) {
        this.ownerIds = ownerIds;
    }

    public int getAmountOfEmployees() {
        return amountOfEmployees;
    }

    public void setAmountOfEmployees(int amountOfEmployees) {
        this.amountOfEmployees = amountOfEmployees;
    }

    public List<StringTagRequestModel> getBusinessStringTags() {
        return businessStringTags;
    }

    public void setBusinessStringTags(List<StringTagRequestModel> businessStringTags) {
        this.businessStringTags = businessStringTags;
    }

    public List<DoubleTagRequestModel> getBusinessDoubleTags() {
        return businessDoubleTags;
    }

    public void setBusinessDoubleTags(List<DoubleTagRequestModel> businessDoubleTags) {
        this.businessDoubleTags = businessDoubleTags;
    }

    public List<StringTagRequestModel> getStringTags() {
        return stringTags;
    }

    public void setStringTags(List<StringTagRequestModel> stringTags) {
        this.stringTags = stringTags;
    }

    public List<DoubleTagRequestModel> getDoubleTags() {
        return doubleTags;
    }

    public void setDoubleTags(List<DoubleTagRequestModel> doubleTags) {
        this.doubleTags = doubleTags;
    }
}
