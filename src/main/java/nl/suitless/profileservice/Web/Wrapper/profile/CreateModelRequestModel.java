package nl.suitless.profileservice.Web.Wrapper.profile;

import javax.validation.constraints.NotNull;

public class CreateModelRequestModel {
    @NotNull
    private String moduleId;
    @NotNull
    private String reportId;

    public CreateModelRequestModel() {
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }
}
