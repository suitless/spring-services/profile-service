package nl.suitless.profileservice.Web.Wrapper.businessInvite;

public class InviteRequestModel {
    private String businessInviteId;

    public InviteRequestModel() {
    }

    public String getBusinessInviteId() {
        return businessInviteId;
    }

    public void setBusinessInviteId(String businessInviteId) {
        this.businessInviteId = businessInviteId;
    }
}
