package nl.suitless.profileservice.Web.Wrapper.businessProfile;

import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateBusinessProfileRequestModel {
    @NotNull
    private String businessName;
    private String templateAssetId;
    private List<String> ownerIds;
    private List<StringTagRequestModel> stringTags;
    private List<DoubleTagRequestModel> doubleTags;

    public CreateBusinessProfileRequestModel() {
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTemplateAssetId() {
        return templateAssetId;
    }

    public void setTemplateAssetId(String templateAssetId) {
        this.templateAssetId = templateAssetId;
    }

    public List<String> getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(List<String> ownerIds) {
        this.ownerIds = ownerIds;
    }

    @Nullable
    public List<StringTagRequestModel> getStringTags() {
        return stringTags;
    }

    public void setStringTags(@Nullable List<StringTagRequestModel> stringTags) {
        this.stringTags = stringTags;
    }

    @Nullable
    public List<DoubleTagRequestModel> getDoubleTags() {
        return doubleTags;
    }

    public void setDoubleTags(@Nullable List<DoubleTagRequestModel> doubleTags) {
        this.doubleTags = doubleTags;
    }
}
