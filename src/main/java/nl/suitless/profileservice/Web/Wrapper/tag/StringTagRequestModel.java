package nl.suitless.profileservice.Web.Wrapper.tag;

import javax.validation.constraints.NotNull;

public class StringTagRequestModel {
    @NotNull
    private String key;
    @NotNull
    private String value;

    public StringTagRequestModel() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
