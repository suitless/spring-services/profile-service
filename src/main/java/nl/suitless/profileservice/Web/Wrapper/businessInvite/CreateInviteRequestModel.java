package nl.suitless.profileservice.Web.Wrapper.businessInvite;

import nl.suitless.profileservice.Domain.Entities.Business.BusinessProfile;

public class CreateInviteRequestModel {
    private String email;
    private BusinessProfile businessProfile;

    public CreateInviteRequestModel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BusinessProfile getBusinessProfile() {
        return businessProfile;
    }

    public void setBusinessProfile(BusinessProfile businessProfile) {
        this.businessProfile = businessProfile;
    }
}
