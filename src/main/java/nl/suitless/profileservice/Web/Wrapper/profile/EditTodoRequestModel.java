package nl.suitless.profileservice.Web.Wrapper.profile;

import javax.validation.constraints.NotNull;

public class EditTodoRequestModel {
    @NotNull
    private String todoType;
    @NotNull
    private String assignment;
    private boolean state;

    public EditTodoRequestModel() {
    }

    public String getTodoType() {
        return todoType;
    }

    public void setTodoType(String todoType) {
        this.todoType = todoType;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
