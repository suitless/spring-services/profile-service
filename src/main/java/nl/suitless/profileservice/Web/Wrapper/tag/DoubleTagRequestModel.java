package nl.suitless.profileservice.Web.Wrapper.tag;

import javax.validation.constraints.NotNull;

public class DoubleTagRequestModel {
    @NotNull
    private String key;
    @NotNull
    private double value;

    public DoubleTagRequestModel() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
