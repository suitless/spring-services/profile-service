package nl.suitless.profileservice.Web.Wrapper.profile;

import nl.suitless.profileservice.Domain.Entities.Profile.Role;
import nl.suitless.profileservice.Web.Wrapper.tag.DoubleTagRequestModel;
import nl.suitless.profileservice.Web.Wrapper.tag.StringTagRequestModel;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateProfileRequestModel {
    @NotNull
    private Role role;
    private List<StringTagRequestModel> stringTags;
    private List<DoubleTagRequestModel> doubleTags;

    public CreateProfileRequestModel() {
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Nullable
    public List<StringTagRequestModel> getStringTags() {
        return stringTags;
    }

    public void setStringTags(List<StringTagRequestModel> stringTags) {
        this.stringTags = stringTags;
    }

    @Nullable
    public List<DoubleTagRequestModel> getDoubleTags() {
        return doubleTags;
    }

    public void setDoubleTags(List<DoubleTagRequestModel> doubleTags) {
        this.doubleTags = doubleTags;
    }
}
