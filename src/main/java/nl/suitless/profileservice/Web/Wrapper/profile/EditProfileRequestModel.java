package nl.suitless.profileservice.Web.Wrapper.profile;

import nl.suitless.profileservice.Domain.Entities.Profile.Role;

import javax.validation.constraints.NotNull;

public class EditProfileRequestModel {
    @NotNull
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
