package nl.suitless.profileservice.Web.Wrapper.businessProfile;

import nl.suitless.profileservice.Domain.Entities.Profile.Role;

public class AddAccountRequestModel {
    private Role role;

    public AddAccountRequestModel() {
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
