FROM openjdk:11
COPY /build/libs/nl.suitless.profile-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8090
CMD "java" "-jar" "run.jar"
